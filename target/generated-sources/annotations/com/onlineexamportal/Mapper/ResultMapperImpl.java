package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.RequestResultDto;
import com.onlineexamportal.dto.ResponseExamDto;
import com.onlineexamportal.dto.ResponseResultDetailDto;
import com.onlineexamportal.dto.ResponseResultDto;
import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.Result;
import com.onlineexamportal.entities.User;
import java.text.SimpleDateFormat;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-04-21T01:02:54+0530",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 1.8.0_392-392 (OpenLogic-OpenJDK)"
)
@Component
public class ResultMapperImpl implements ResultMapper {

    @Override
    public RequestResultDto toRequestResultDto(Result result) {
        if ( result == null ) {
            return null;
        }

        RequestResultDto requestResultDto = new RequestResultDto();

        requestResultDto.setUserId( resultUserUserId( result ) );
        requestResultDto.setExamId( resultExamExamId( result ) );

        return requestResultDto;
    }

    @Override
    public ResponseResultDetailDto toResponseResultDetailDto(Result result) {
        if ( result == null ) {
            return null;
        }

        ResponseResultDetailDto responseResultDetailDto = new ResponseResultDetailDto();

        if ( result.getUser() != null ) {
            if ( responseResultDetailDto.getResponseResultDto() == null ) {
                responseResultDetailDto.setResponseResultDto( new ResponseResultDto() );
            }
            userToResponseResultDto( result.getUser(), responseResultDetailDto.getResponseResultDto() );
        }
        if ( result.getExam() != null ) {
            if ( responseResultDetailDto.getResponseResultDto() == null ) {
                responseResultDetailDto.setResponseResultDto( new ResponseResultDto() );
            }
            examToResponseResultDto( result.getExam(), responseResultDetailDto.getResponseResultDto() );
        }
        if ( responseResultDetailDto.getResponseResultDto() == null ) {
            responseResultDetailDto.setResponseResultDto( new ResponseResultDto() );
        }
        resultToResponseResultDto( result, responseResultDetailDto.getResponseResultDto() );
        responseResultDetailDto.setResponseExamDto( examToResponseExamDto( result.getExam() ) );

        return responseResultDetailDto;
    }

    @Override
    public ResponseResultDto toResponseResultDto(Result result) {
        if ( result == null ) {
            return null;
        }

        ResponseResultDto responseResultDto = new ResponseResultDto();

        responseResultDto.setUserName( resultUserName( result ) );
        responseResultDto.setUserId( resultUserUserId( result ) );
        responseResultDto.setExamId( resultExamExamId( result ) );
        responseResultDto.setResultId( result.getResultId() );
        responseResultDto.setLogicalMarks( result.getLogicalMarks() );
        responseResultDto.setTechnicalMarks( result.getTechnicalMarks() );
        responseResultDto.setProgrammingMarks( result.getProgrammingMarks() );
        responseResultDto.setLogicalStatus( result.isLogicalStatus() );
        responseResultDto.setTechnicalStatus( result.isTechnicalStatus() );
        responseResultDto.setProgrammingStatus( result.isProgrammingStatus() );
        responseResultDto.setStatus( result.isStatus() );

        return responseResultDto;
    }

    @Override
    public Result toRequestResultEntity(RequestResultDto requestResultDto) {
        if ( requestResultDto == null ) {
            return null;
        }

        Result result = new Result();

        result.setUser( requestResultDtoToUser( requestResultDto ) );
        result.setExam( requestResultDtoToExam( requestResultDto ) );

        return result;
    }

    @Override
    public Result toResponseResultEntity(ResponseResultDto responseResultDto) {
        if ( responseResultDto == null ) {
            return null;
        }

        Result result = new Result();

        result.setUser( responseResultDtoToUser( responseResultDto ) );
        result.setExam( responseResultDtoToExam( responseResultDto ) );
        result.setResultId( responseResultDto.getResultId() );
        result.setLogicalMarks( responseResultDto.getLogicalMarks() );
        result.setTechnicalMarks( responseResultDto.getTechnicalMarks() );
        result.setProgrammingMarks( responseResultDto.getProgrammingMarks() );
        result.setLogicalStatus( responseResultDto.isLogicalStatus() );
        result.setTechnicalStatus( responseResultDto.isTechnicalStatus() );
        result.setProgrammingStatus( responseResultDto.isProgrammingStatus() );
        result.setStatus( responseResultDto.isStatus() );

        return result;
    }

    private Long resultUserUserId(Result result) {
        if ( result == null ) {
            return null;
        }
        User user = result.getUser();
        if ( user == null ) {
            return null;
        }
        Long userId = user.getUserId();
        if ( userId == null ) {
            return null;
        }
        return userId;
    }

    private Long resultExamExamId(Result result) {
        if ( result == null ) {
            return null;
        }
        Exam exam = result.getExam();
        if ( exam == null ) {
            return null;
        }
        Long examId = exam.getExamId();
        if ( examId == null ) {
            return null;
        }
        return examId;
    }

    protected void userToResponseResultDto(User user, ResponseResultDto mappingTarget) {
        if ( user == null ) {
            return;
        }

        mappingTarget.setUserName( user.getUserName() );
        mappingTarget.setUserId( user.getUserId() );
    }

    protected void examToResponseResultDto(Exam exam, ResponseResultDto mappingTarget) {
        if ( exam == null ) {
            return;
        }

        mappingTarget.setExamId( exam.getExamId() );
    }

    protected void resultToResponseResultDto(Result result, ResponseResultDto mappingTarget) {
        if ( result == null ) {
            return;
        }

        mappingTarget.setResultId( result.getResultId() );
        mappingTarget.setLogicalMarks( result.getLogicalMarks() );
        mappingTarget.setTechnicalMarks( result.getTechnicalMarks() );
        mappingTarget.setProgrammingMarks( result.getProgrammingMarks() );
        mappingTarget.setLogicalStatus( result.isLogicalStatus() );
        mappingTarget.setTechnicalStatus( result.isTechnicalStatus() );
        mappingTarget.setProgrammingStatus( result.isProgrammingStatus() );
        mappingTarget.setStatus( result.isStatus() );
    }

    private String examUserUserName(Exam exam) {
        if ( exam == null ) {
            return null;
        }
        User user = exam.getUser();
        if ( user == null ) {
            return null;
        }
        String userName = user.getUserName();
        if ( userName == null ) {
            return null;
        }
        return userName;
    }

    protected ResponseExamDto examToResponseExamDto(Exam exam) {
        if ( exam == null ) {
            return null;
        }

        ResponseExamDto responseExamDto = new ResponseExamDto();

        responseExamDto.setExamName( exam.getExamName() );
        responseExamDto.setTotalMarks( exam.getTotalMarks() );
        responseExamDto.setExamId( exam.getExamId() );
        responseExamDto.setUserName( examUserUserName( exam ) );
        responseExamDto.setDuration( exam.getDuration() );
        responseExamDto.setNoOfLogicalQuestion( exam.getNoOfLogicalQuestion() );
        responseExamDto.setNoOfTechnicalQuestion( exam.getNoOfTechnicalQuestion() );
        responseExamDto.setNoOfProgrammingQuestion( exam.getNoOfProgrammingQuestion() );
        if ( exam.getDate() != null ) {
            responseExamDto.setDate( new SimpleDateFormat().format( exam.getDate() ) );
        }
        responseExamDto.setStartDate( exam.getStartDate() );
        responseExamDto.setEndDate( exam.getEndDate() );
        responseExamDto.setTechnicalPassingMarks( exam.getTechnicalPassingMarks() );
        responseExamDto.setLogicalPassingMarks( exam.getLogicalPassingMarks() );
        responseExamDto.setProgrammingPassingMarks( exam.getProgrammingPassingMarks() );
        responseExamDto.setStartTime( exam.getStartTime() );
        responseExamDto.setEndTime( exam.getEndTime() );

        return responseExamDto;
    }

    private String resultUserName(Result result) {
        if ( result == null ) {
            return null;
        }
        User user = result.getUser();
        if ( user == null ) {
            return null;
        }
        String name = user.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    protected User requestResultDtoToUser(RequestResultDto requestResultDto) {
        if ( requestResultDto == null ) {
            return null;
        }

        User user = new User();

        user.setUserId( requestResultDto.getUserId() );

        return user;
    }

    protected Exam requestResultDtoToExam(RequestResultDto requestResultDto) {
        if ( requestResultDto == null ) {
            return null;
        }

        Exam exam = new Exam();

        exam.setExamId( requestResultDto.getExamId() );

        return exam;
    }

    protected User responseResultDtoToUser(ResponseResultDto responseResultDto) {
        if ( responseResultDto == null ) {
            return null;
        }

        User user = new User();

        user.setName( responseResultDto.getUserName() );
        user.setUserId( responseResultDto.getUserId() );

        return user;
    }

    protected Exam responseResultDtoToExam(ResponseResultDto responseResultDto) {
        if ( responseResultDto == null ) {
            return null;
        }

        Exam exam = new Exam();

        exam.setExamId( responseResultDto.getExamId() );

        return exam;
    }
}
