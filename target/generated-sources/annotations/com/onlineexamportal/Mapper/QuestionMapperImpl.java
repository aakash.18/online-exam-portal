package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.dto.ResponseQuestionDto;
import com.onlineexamportal.entities.Category;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.QuestionImage;
import com.onlineexamportal.entities.User;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-04-21T01:02:54+0530",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 1.8.0_392-392 (OpenLogic-OpenJDK)"
)
@Component
public class QuestionMapperImpl implements QuestionMapper {

    @Override
    public QuestionDto toDto(Question question) {
        if ( question == null ) {
            return null;
        }

        QuestionDto questionDto = new QuestionDto();

        questionDto.setAdminId( questionUserUserId( question ) );
        String categoryName = questionCategoryCategoryName( question );
        if ( categoryName != null ) {
            questionDto.setTypeOfCategory( Long.parseLong( categoryName ) );
        }
        questionDto.setImageUrl( questionQuestionImageImageUrl( question ) );
        questionDto.setOpt1ImageUrl( questionQuestionImageOpt1ImageUrl( question ) );
        questionDto.setOpt2ImageUrl( questionQuestionImageOpt2ImageUrl( question ) );
        questionDto.setOpt3ImageUrl( questionQuestionImageOpt3ImageUrl( question ) );
        questionDto.setOpt4ImageUrl( questionQuestionImageOpt4ImageUrl( question ) );
        questionDto.setQuestion( question.getQuestion() );
        questionDto.setOpt1( question.getOpt1() );
        questionDto.setOpt2( question.getOpt2() );
        questionDto.setOpt3( question.getOpt3() );
        questionDto.setOpt4( question.getOpt4() );
        questionDto.setCorrectAns( question.getCorrectAns() );
        questionDto.setDifficulty( question.getDifficulty() );
        questionDto.setImgUrls( question.getImgUrls() );
        questionDto.setMark( question.getMark() );

        return questionDto;
    }

    @Override
    public ResponseQuestionDto toResponseQuestionDto(Question question) {
        if ( question == null ) {
            return null;
        }

        ResponseQuestionDto responseQuestionDto = new ResponseQuestionDto();

        responseQuestionDto.setAdmin( questionUserUserName( question ) );
        responseQuestionDto.setTypeOfCategory( questionCategoryCategoryName( question ) );
        responseQuestionDto.setImageUrl( questionQuestionImageImageUrl( question ) );
        responseQuestionDto.setOpt1ImageUrl( questionQuestionImageOpt1ImageUrl( question ) );
        responseQuestionDto.setOpt2ImageUrl( questionQuestionImageOpt2ImageUrl( question ) );
        responseQuestionDto.setOpt3ImageUrl( questionQuestionImageOpt3ImageUrl( question ) );
        responseQuestionDto.setOpt4ImageUrl( questionQuestionImageOpt4ImageUrl( question ) );
        responseQuestionDto.setQuestion( question.getQuestion() );
        responseQuestionDto.setOpt1( question.getOpt1() );
        responseQuestionDto.setOpt2( question.getOpt2() );
        responseQuestionDto.setOpt3( question.getOpt3() );
        responseQuestionDto.setOpt4( question.getOpt4() );
        responseQuestionDto.setCorrectAns( question.getCorrectAns() );
        responseQuestionDto.setDifficulty( question.getDifficulty() );
        responseQuestionDto.setImgUrls( question.getImgUrls() );
        responseQuestionDto.setMark( question.getMark() );
        responseQuestionDto.setQuestionId( question.getQuestionId() );

        return responseQuestionDto;
    }

    @Override
    public List<ResponseQuestionDto> toResponseQuestionDtoList(List<Question> questionList) {
        if ( questionList == null ) {
            return null;
        }

        List<ResponseQuestionDto> list = new ArrayList<ResponseQuestionDto>( questionList.size() );
        for ( Question question : questionList ) {
            list.add( toResponseQuestionDto( question ) );
        }

        return list;
    }

    @Override
    public Question toEntity(QuestionDto questionDto) {
        if ( questionDto == null ) {
            return null;
        }

        Question question = new Question();

        question.setQuestion( questionDto.getQuestion() );
        question.setOpt1( questionDto.getOpt1() );
        question.setOpt2( questionDto.getOpt2() );
        question.setOpt3( questionDto.getOpt3() );
        question.setOpt4( questionDto.getOpt4() );
        question.setCorrectAns( questionDto.getCorrectAns() );
        question.setMark( questionDto.getMark() );
        question.setDifficulty( questionDto.getDifficulty() );
        question.setImgUrls( questionDto.getImgUrls() );

        return question;
    }

    private Long questionUserUserId(Question question) {
        if ( question == null ) {
            return null;
        }
        User user = question.getUser();
        if ( user == null ) {
            return null;
        }
        Long userId = user.getUserId();
        if ( userId == null ) {
            return null;
        }
        return userId;
    }

    private String questionCategoryCategoryName(Question question) {
        if ( question == null ) {
            return null;
        }
        Category category = question.getCategory();
        if ( category == null ) {
            return null;
        }
        String categoryName = category.getCategoryName();
        if ( categoryName == null ) {
            return null;
        }
        return categoryName;
    }

    private String questionQuestionImageImageUrl(Question question) {
        if ( question == null ) {
            return null;
        }
        QuestionImage questionImage = question.getQuestionImage();
        if ( questionImage == null ) {
            return null;
        }
        String imageUrl = questionImage.getImageUrl();
        if ( imageUrl == null ) {
            return null;
        }
        return imageUrl;
    }

    private String questionQuestionImageOpt1ImageUrl(Question question) {
        if ( question == null ) {
            return null;
        }
        QuestionImage questionImage = question.getQuestionImage();
        if ( questionImage == null ) {
            return null;
        }
        String opt1ImageUrl = questionImage.getOpt1ImageUrl();
        if ( opt1ImageUrl == null ) {
            return null;
        }
        return opt1ImageUrl;
    }

    private String questionQuestionImageOpt2ImageUrl(Question question) {
        if ( question == null ) {
            return null;
        }
        QuestionImage questionImage = question.getQuestionImage();
        if ( questionImage == null ) {
            return null;
        }
        String opt2ImageUrl = questionImage.getOpt2ImageUrl();
        if ( opt2ImageUrl == null ) {
            return null;
        }
        return opt2ImageUrl;
    }

    private String questionQuestionImageOpt3ImageUrl(Question question) {
        if ( question == null ) {
            return null;
        }
        QuestionImage questionImage = question.getQuestionImage();
        if ( questionImage == null ) {
            return null;
        }
        String opt3ImageUrl = questionImage.getOpt3ImageUrl();
        if ( opt3ImageUrl == null ) {
            return null;
        }
        return opt3ImageUrl;
    }

    private String questionQuestionImageOpt4ImageUrl(Question question) {
        if ( question == null ) {
            return null;
        }
        QuestionImage questionImage = question.getQuestionImage();
        if ( questionImage == null ) {
            return null;
        }
        String opt4ImageUrl = questionImage.getOpt4ImageUrl();
        if ( opt4ImageUrl == null ) {
            return null;
        }
        return opt4ImageUrl;
    }

    private String questionUserUserName(Question question) {
        if ( question == null ) {
            return null;
        }
        User user = question.getUser();
        if ( user == null ) {
            return null;
        }
        String userName = user.getUserName();
        if ( userName == null ) {
            return null;
        }
        return userName;
    }
}
