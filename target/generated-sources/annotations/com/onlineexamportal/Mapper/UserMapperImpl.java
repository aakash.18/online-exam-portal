package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.ResponseExamDto;
import com.onlineexamportal.dto.ResponseUserDto;
import com.onlineexamportal.dto.ResponseUserStudentDto;
import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.User;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-04-21T01:02:54+0530",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 1.8.0_392-392 (OpenLogic-OpenJDK)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public ResponseUserDto toDto(User user) {
        if ( user == null ) {
            return null;
        }

        ResponseUserDto responseUserDto = new ResponseUserDto();

        responseUserDto.setResponseExamDtoList( examListToResponseExamDtoList( user.getExamStudentList() ) );
        responseUserDto.setUserId( user.getUserId() );
        responseUserDto.setUserName( user.getUserName() );
        responseUserDto.setPassword( user.getPassword() );
        responseUserDto.setName( user.getName() );
        responseUserDto.setUserRole( user.getUserRole() );

        return responseUserDto;
    }

    @Override
    public ResponseUserStudentDto toResponseUserStudentDto(User user) {
        if ( user == null ) {
            return null;
        }

        ResponseUserStudentDto responseUserStudentDto = new ResponseUserStudentDto();

        responseUserStudentDto.setUserId( user.getUserId() );
        responseUserStudentDto.setUserName( user.getUserName() );
        responseUserStudentDto.setName( user.getName() );
        responseUserStudentDto.setUserRole( user.getUserRole() );
        responseUserStudentDto.setActive( user.isActive() );
        responseUserStudentDto.setPassword( user.getPassword() );

        return responseUserStudentDto;
    }

    @Override
    public User toEntity(ResponseUserDto responseUserDto) {
        if ( responseUserDto == null ) {
            return null;
        }

        User user = new User();

        user.setUserId( responseUserDto.getUserId() );
        user.setUserName( responseUserDto.getUserName() );
        user.setPassword( responseUserDto.getPassword() );
        user.setName( responseUserDto.getName() );
        user.setUserRole( responseUserDto.getUserRole() );

        return user;
    }

    protected ResponseExamDto examToResponseExamDto(Exam exam) {
        if ( exam == null ) {
            return null;
        }

        ResponseExamDto responseExamDto = new ResponseExamDto();

        responseExamDto.setExamId( exam.getExamId() );
        responseExamDto.setExamName( exam.getExamName() );
        responseExamDto.setDuration( exam.getDuration() );
        responseExamDto.setNoOfLogicalQuestion( exam.getNoOfLogicalQuestion() );
        responseExamDto.setNoOfTechnicalQuestion( exam.getNoOfTechnicalQuestion() );
        responseExamDto.setNoOfProgrammingQuestion( exam.getNoOfProgrammingQuestion() );
        if ( exam.getDate() != null ) {
            responseExamDto.setDate( new SimpleDateFormat().format( exam.getDate() ) );
        }
        responseExamDto.setStartDate( exam.getStartDate() );
        responseExamDto.setEndDate( exam.getEndDate() );
        responseExamDto.setTotalMarks( exam.getTotalMarks() );
        responseExamDto.setTechnicalPassingMarks( exam.getTechnicalPassingMarks() );
        responseExamDto.setLogicalPassingMarks( exam.getLogicalPassingMarks() );
        responseExamDto.setProgrammingPassingMarks( exam.getProgrammingPassingMarks() );
        responseExamDto.setStartTime( exam.getStartTime() );
        responseExamDto.setEndTime( exam.getEndTime() );

        return responseExamDto;
    }

    protected List<ResponseExamDto> examListToResponseExamDtoList(List<Exam> list) {
        if ( list == null ) {
            return null;
        }

        List<ResponseExamDto> list1 = new ArrayList<ResponseExamDto>( list.size() );
        for ( Exam exam : list ) {
            list1.add( examToResponseExamDto( exam ) );
        }

        return list1;
    }
}
