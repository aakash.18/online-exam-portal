package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.RequestCategoryDto;
import com.onlineexamportal.dto.ResponseCategoryDto;
import com.onlineexamportal.entities.Category;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-04-21T01:02:54+0530",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 1.8.0_392-392 (OpenLogic-OpenJDK)"
)
@Component
public class CategoryMapperImpl implements CategoryMapper {

    @Override
    public RequestCategoryDto toRequestCategoryDto(Category category) {
        if ( category == null ) {
            return null;
        }

        RequestCategoryDto requestCategoryDto = new RequestCategoryDto();

        requestCategoryDto.setCategoryName( category.getCategoryName() );

        return requestCategoryDto;
    }

    @Override
    public ResponseCategoryDto toResponseCategoryDto(Category category) {
        if ( category == null ) {
            return null;
        }

        ResponseCategoryDto responseCategoryDto = new ResponseCategoryDto();

        responseCategoryDto.setCategoryId( category.getCategoryId() );
        responseCategoryDto.setCategoryName( category.getCategoryName() );

        return responseCategoryDto;
    }

    @Override
    public Category toRequestEntity(RequestCategoryDto requestCategoryDto) {
        if ( requestCategoryDto == null ) {
            return null;
        }

        Category category = new Category();

        category.setCategoryName( requestCategoryDto.getCategoryName() );

        return category;
    }

    @Override
    public Category toResponseEntity(ResponseCategoryDto responseCategoryDto) {
        if ( responseCategoryDto == null ) {
            return null;
        }

        Category category = new Category();

        category.setCategoryId( responseCategoryDto.getCategoryId() );
        category.setCategoryName( responseCategoryDto.getCategoryName() );

        return category;
    }
}
