package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.RequestStudentSelectedAnswerDto;
import com.onlineexamportal.dto.ResponseQuestionDto;
import com.onlineexamportal.dto.ResponseSelectedAnswerByCategoryDto;
import com.onlineexamportal.dto.ResponseStudentSelectedAnswerDto;
import com.onlineexamportal.dto.ResponseUserDto;
import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.StudentSelectedAnswer;
import com.onlineexamportal.entities.User;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-04-21T01:02:54+0530",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 1.8.0_392-392 (OpenLogic-OpenJDK)"
)
@Component
public class StudentSelectedAnswerMapperImpl implements StudentSelectedAnswerMapper {

    @Override
    public ResponseStudentSelectedAnswerDto toResponseStudentSelectedAnswerDto(StudentSelectedAnswer studentSelectedAnswer) {
        if ( studentSelectedAnswer == null ) {
            return null;
        }

        ResponseStudentSelectedAnswerDto responseStudentSelectedAnswerDto = new ResponseStudentSelectedAnswerDto();

        responseStudentSelectedAnswerDto.setExamId( studentSelectedAnswerExamExamId( studentSelectedAnswer ) );
        responseStudentSelectedAnswerDto.setUserId( studentSelectedAnswerUserUserId( studentSelectedAnswer ) );
        responseStudentSelectedAnswerDto.setQuestionId( studentSelectedAnswerQuestionQuestionId( studentSelectedAnswer ) );
        responseStudentSelectedAnswerDto.setStudentSelectedAnswerId( studentSelectedAnswer.getStudentSelectedAnswerId() );
        responseStudentSelectedAnswerDto.setSelectedAnswer( studentSelectedAnswer.getSelectedAnswer() );

        return responseStudentSelectedAnswerDto;
    }

    @Override
    public RequestStudentSelectedAnswerDto toRequestStudentSelectedAnswerDto(StudentSelectedAnswer studentSelectedAnswer) {
        if ( studentSelectedAnswer == null ) {
            return null;
        }

        RequestStudentSelectedAnswerDto requestStudentSelectedAnswerDto = new RequestStudentSelectedAnswerDto();

        requestStudentSelectedAnswerDto.setExamId( studentSelectedAnswerExamExamId( studentSelectedAnswer ) );
        requestStudentSelectedAnswerDto.setUserId( studentSelectedAnswerUserUserId( studentSelectedAnswer ) );
        requestStudentSelectedAnswerDto.setQuestionId( studentSelectedAnswerQuestionQuestionId( studentSelectedAnswer ) );
        requestStudentSelectedAnswerDto.setSelectedAnswer( studentSelectedAnswer.getSelectedAnswer() );

        return requestStudentSelectedAnswerDto;
    }

    @Override
    public ResponseSelectedAnswerByCategoryDto toResponseSelectedAnswerByCategoryDto(StudentSelectedAnswer studentSelectedAnswer) {
        if ( studentSelectedAnswer == null ) {
            return null;
        }

        ResponseSelectedAnswerByCategoryDto responseSelectedAnswerByCategoryDto = new ResponseSelectedAnswerByCategoryDto();

        responseSelectedAnswerByCategoryDto.setExamId( studentSelectedAnswerExamExamId( studentSelectedAnswer ) );
        responseSelectedAnswerByCategoryDto.setUserDto( userToResponseUserDto( studentSelectedAnswer.getUser() ) );
        responseSelectedAnswerByCategoryDto.setQuestionDto( questionToResponseQuestionDto( studentSelectedAnswer.getQuestion() ) );
        responseSelectedAnswerByCategoryDto.setStudentSelectedAnswerId( studentSelectedAnswer.getStudentSelectedAnswerId() );
        responseSelectedAnswerByCategoryDto.setSelectedAnswer( studentSelectedAnswer.getSelectedAnswer() );

        return responseSelectedAnswerByCategoryDto;
    }

    private Long studentSelectedAnswerExamExamId(StudentSelectedAnswer studentSelectedAnswer) {
        if ( studentSelectedAnswer == null ) {
            return null;
        }
        Exam exam = studentSelectedAnswer.getExam();
        if ( exam == null ) {
            return null;
        }
        Long examId = exam.getExamId();
        if ( examId == null ) {
            return null;
        }
        return examId;
    }

    private Long studentSelectedAnswerUserUserId(StudentSelectedAnswer studentSelectedAnswer) {
        if ( studentSelectedAnswer == null ) {
            return null;
        }
        User user = studentSelectedAnswer.getUser();
        if ( user == null ) {
            return null;
        }
        Long userId = user.getUserId();
        if ( userId == null ) {
            return null;
        }
        return userId;
    }

    private Long studentSelectedAnswerQuestionQuestionId(StudentSelectedAnswer studentSelectedAnswer) {
        if ( studentSelectedAnswer == null ) {
            return null;
        }
        Question question = studentSelectedAnswer.getQuestion();
        if ( question == null ) {
            return null;
        }
        Long questionId = question.getQuestionId();
        if ( questionId == null ) {
            return null;
        }
        return questionId;
    }

    protected ResponseUserDto userToResponseUserDto(User user) {
        if ( user == null ) {
            return null;
        }

        ResponseUserDto responseUserDto = new ResponseUserDto();

        responseUserDto.setUserId( user.getUserId() );
        responseUserDto.setUserName( user.getUserName() );
        responseUserDto.setPassword( user.getPassword() );
        responseUserDto.setName( user.getName() );
        responseUserDto.setUserRole( user.getUserRole() );

        return responseUserDto;
    }

    protected ResponseQuestionDto questionToResponseQuestionDto(Question question) {
        if ( question == null ) {
            return null;
        }

        ResponseQuestionDto responseQuestionDto = new ResponseQuestionDto();

        responseQuestionDto.setQuestion( question.getQuestion() );
        responseQuestionDto.setOpt1( question.getOpt1() );
        responseQuestionDto.setOpt2( question.getOpt2() );
        responseQuestionDto.setOpt3( question.getOpt3() );
        responseQuestionDto.setOpt4( question.getOpt4() );
        responseQuestionDto.setCorrectAns( question.getCorrectAns() );
        responseQuestionDto.setDifficulty( question.getDifficulty() );
        responseQuestionDto.setImgUrls( question.getImgUrls() );
        responseQuestionDto.setMark( question.getMark() );
        responseQuestionDto.setQuestionId( question.getQuestionId() );

        return responseQuestionDto;
    }
}
