package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.RequestExamDto;
import com.onlineexamportal.dto.RequestExamStudentDto;
import com.onlineexamportal.dto.RequestResultDto;
import com.onlineexamportal.dto.ResponseExamDto;
import com.onlineexamportal.dto.ResponseQuestionDto;
import com.onlineexamportal.dto.ResponseUserDto;
import com.onlineexamportal.entities.Category;
import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.Result;
import com.onlineexamportal.entities.User;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-04-21T01:02:54+0530",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 1.8.0_392-392 (OpenLogic-OpenJDK)"
)
@Component
public class ExamMapperImpl implements ExamMapper {

    @Override
    public ResponseExamDto toResponseExamDto(Exam exam) {
        if ( exam == null ) {
            return null;
        }

        ResponseExamDto responseExamDto = new ResponseExamDto();

        responseExamDto.setUserName( examUserUserName( exam ) );
        responseExamDto.setRequestResultDtoList( resultListToRequestResultDtoList( exam.getResultList() ) );
        responseExamDto.setLogicalDifficulty( exam.getLogicalQuestionDifficulty() );
        responseExamDto.setTechnicalDifficulty( exam.getTechnicalQuestionDifficulty() );
        responseExamDto.setProgrammingDifficulty( exam.getProgrammingQuestionDifficulty() );
        responseExamDto.setQuestionDtoList( toResponseQuestionDtoList( exam.getExamQuestionList() ) );
        responseExamDto.setResponseUserDtoList( userListToResponseUserDtoList( exam.getExamStudentList() ) );
        responseExamDto.setExamId( exam.getExamId() );
        responseExamDto.setExamName( exam.getExamName() );
        responseExamDto.setDuration( exam.getDuration() );
        responseExamDto.setNoOfLogicalQuestion( exam.getNoOfLogicalQuestion() );
        responseExamDto.setNoOfTechnicalQuestion( exam.getNoOfTechnicalQuestion() );
        responseExamDto.setNoOfProgrammingQuestion( exam.getNoOfProgrammingQuestion() );
        if ( exam.getDate() != null ) {
            responseExamDto.setDate( new SimpleDateFormat().format( exam.getDate() ) );
        }
        responseExamDto.setStartDate( exam.getStartDate() );
        responseExamDto.setEndDate( exam.getEndDate() );
        responseExamDto.setTotalMarks( exam.getTotalMarks() );
        responseExamDto.setTechnicalPassingMarks( exam.getTechnicalPassingMarks() );
        responseExamDto.setLogicalPassingMarks( exam.getLogicalPassingMarks() );
        responseExamDto.setProgrammingPassingMarks( exam.getProgrammingPassingMarks() );
        responseExamDto.setStartTime( exam.getStartTime() );
        responseExamDto.setEndTime( exam.getEndTime() );

        return responseExamDto;
    }

    @Override
    public ResponseQuestionDto toResponseQuestionDto(Question question) {
        if ( question == null ) {
            return null;
        }

        ResponseQuestionDto responseQuestionDto = new ResponseQuestionDto();

        responseQuestionDto.setAdmin( questionUserUserName( question ) );
        responseQuestionDto.setTypeOfCategory( questionCategoryCategoryName( question ) );
        responseQuestionDto.setQuestion( question.getQuestion() );
        responseQuestionDto.setOpt1( question.getOpt1() );
        responseQuestionDto.setOpt2( question.getOpt2() );
        responseQuestionDto.setOpt3( question.getOpt3() );
        responseQuestionDto.setOpt4( question.getOpt4() );
        responseQuestionDto.setCorrectAns( question.getCorrectAns() );
        responseQuestionDto.setDifficulty( question.getDifficulty() );
        responseQuestionDto.setImgUrls( question.getImgUrls() );
        responseQuestionDto.setMark( question.getMark() );
        responseQuestionDto.setQuestionId( question.getQuestionId() );

        return responseQuestionDto;
    }

    @Override
    public RequestExamStudentDto toRequestExamStudentDto(Exam exam) {
        if ( exam == null ) {
            return null;
        }

        RequestExamStudentDto requestExamStudentDto = new RequestExamStudentDto();

        List<User> list = exam.getExamStudentList();
        if ( list != null ) {
            requestExamStudentDto.setExamStudentList( new ArrayList<User>( list ) );
        }

        return requestExamStudentDto;
    }

    @Override
    public List<ResponseQuestionDto> toResponseQuestionDtoList(List<Question> questionList) {
        if ( questionList == null ) {
            return null;
        }

        List<ResponseQuestionDto> list = new ArrayList<ResponseQuestionDto>( questionList.size() );
        for ( Question question : questionList ) {
            list.add( toResponseQuestionDto( question ) );
        }

        return list;
    }

    @Override
    public RequestExamDto toRequestExamDto(Exam exam) {
        if ( exam == null ) {
            return null;
        }

        RequestExamDto requestExamDto = new RequestExamDto();

        requestExamDto.setDuration( exam.getDuration() );
        requestExamDto.setNoOfLogicalQuestion( exam.getNoOfLogicalQuestion() );
        requestExamDto.setNoOfTechnicalQuestion( exam.getNoOfTechnicalQuestion() );
        requestExamDto.setNoOfProgrammingQuestion( exam.getNoOfProgrammingQuestion() );
        requestExamDto.setExamName( exam.getExamName() );
        requestExamDto.setDate( exam.getDate() );
        requestExamDto.setStartDate( exam.getStartDate() );
        requestExamDto.setEndDate( exam.getEndDate() );
        requestExamDto.setTotalMarks( exam.getTotalMarks() );
        requestExamDto.setTechnicalPassingMarks( exam.getTechnicalPassingMarks() );
        requestExamDto.setLogicalPassingMarks( exam.getLogicalPassingMarks() );
        requestExamDto.setProgrammingPassingMarks( exam.getProgrammingPassingMarks() );
        requestExamDto.setStartTime( exam.getStartTime() );
        requestExamDto.setEndTime( exam.getEndTime() );

        return requestExamDto;
    }

    @Override
    public Exam toRequestExamDto(RequestExamDto requestExamDto) {
        if ( requestExamDto == null ) {
            return null;
        }

        Exam exam = new Exam();

        exam.setExamName( requestExamDto.getExamName() );
        exam.setNoOfLogicalQuestion( requestExamDto.getNoOfLogicalQuestion() );
        exam.setNoOfTechnicalQuestion( requestExamDto.getNoOfTechnicalQuestion() );
        exam.setNoOfProgrammingQuestion( requestExamDto.getNoOfProgrammingQuestion() );
        exam.setLogicalPassingMarks( requestExamDto.getLogicalPassingMarks() );
        exam.setTechnicalPassingMarks( requestExamDto.getTechnicalPassingMarks() );
        exam.setProgrammingPassingMarks( requestExamDto.getProgrammingPassingMarks() );
        exam.setDuration( requestExamDto.getDuration() );
        exam.setStartTime( requestExamDto.getStartTime() );
        exam.setEndTime( requestExamDto.getEndTime() );
        exam.setDate( requestExamDto.getDate() );
        exam.setTotalMarks( requestExamDto.getTotalMarks() );
        exam.setStartDate( requestExamDto.getStartDate() );
        exam.setEndDate( requestExamDto.getEndDate() );

        return exam;
    }

    @Override
    public Exam toResponseExamDto(ResponseExamDto responseExamDto) {
        if ( responseExamDto == null ) {
            return null;
        }

        Exam exam = new Exam();

        exam.setExamId( responseExamDto.getExamId() );
        exam.setExamName( responseExamDto.getExamName() );
        exam.setNoOfLogicalQuestion( responseExamDto.getNoOfLogicalQuestion() );
        exam.setNoOfTechnicalQuestion( responseExamDto.getNoOfTechnicalQuestion() );
        exam.setNoOfProgrammingQuestion( responseExamDto.getNoOfProgrammingQuestion() );
        exam.setLogicalPassingMarks( responseExamDto.getLogicalPassingMarks() );
        exam.setTechnicalPassingMarks( responseExamDto.getTechnicalPassingMarks() );
        exam.setProgrammingPassingMarks( responseExamDto.getProgrammingPassingMarks() );
        exam.setDuration( responseExamDto.getDuration() );
        exam.setStartTime( responseExamDto.getStartTime() );
        exam.setEndTime( responseExamDto.getEndTime() );
        try {
            if ( responseExamDto.getDate() != null ) {
                exam.setDate( new SimpleDateFormat().parse( responseExamDto.getDate() ) );
            }
        }
        catch ( ParseException e ) {
            throw new RuntimeException( e );
        }
        exam.setTotalMarks( responseExamDto.getTotalMarks() );
        exam.setStartDate( responseExamDto.getStartDate() );
        exam.setEndDate( responseExamDto.getEndDate() );

        return exam;
    }

    private String examUserUserName(Exam exam) {
        if ( exam == null ) {
            return null;
        }
        User user = exam.getUser();
        if ( user == null ) {
            return null;
        }
        String userName = user.getUserName();
        if ( userName == null ) {
            return null;
        }
        return userName;
    }

    protected RequestResultDto resultToRequestResultDto(Result result) {
        if ( result == null ) {
            return null;
        }

        RequestResultDto requestResultDto = new RequestResultDto();

        return requestResultDto;
    }

    protected List<RequestResultDto> resultListToRequestResultDtoList(List<Result> list) {
        if ( list == null ) {
            return null;
        }

        List<RequestResultDto> list1 = new ArrayList<RequestResultDto>( list.size() );
        for ( Result result : list ) {
            list1.add( resultToRequestResultDto( result ) );
        }

        return list1;
    }

    protected ResponseUserDto userToResponseUserDto(User user) {
        if ( user == null ) {
            return null;
        }

        ResponseUserDto responseUserDto = new ResponseUserDto();

        responseUserDto.setUserId( user.getUserId() );
        responseUserDto.setUserName( user.getUserName() );
        responseUserDto.setPassword( user.getPassword() );
        responseUserDto.setName( user.getName() );
        responseUserDto.setUserRole( user.getUserRole() );

        return responseUserDto;
    }

    protected List<ResponseUserDto> userListToResponseUserDtoList(List<User> list) {
        if ( list == null ) {
            return null;
        }

        List<ResponseUserDto> list1 = new ArrayList<ResponseUserDto>( list.size() );
        for ( User user : list ) {
            list1.add( userToResponseUserDto( user ) );
        }

        return list1;
    }

    private String questionUserUserName(Question question) {
        if ( question == null ) {
            return null;
        }
        User user = question.getUser();
        if ( user == null ) {
            return null;
        }
        String userName = user.getUserName();
        if ( userName == null ) {
            return null;
        }
        return userName;
    }

    private String questionCategoryCategoryName(Question question) {
        if ( question == null ) {
            return null;
        }
        Category category = question.getCategory();
        if ( category == null ) {
            return null;
        }
        String categoryName = category.getCategoryName();
        if ( categoryName == null ) {
            return null;
        }
        return categoryName;
    }
}
