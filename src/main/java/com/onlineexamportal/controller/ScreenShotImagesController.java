package com.onlineexamportal.controller;

import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.services.impl.ScreenShotImageStorageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/screen-shot-images")
@CrossOrigin(origins = "http://localhost:4200")
public class ScreenShotImagesController {

    @Autowired
    private ScreenShotImageStorageServiceImpl screenShotImageStorageServiceImpl;
    @GetMapping("/{fileName}")
    public ResponseEntity<SuccessResponseDTO> downloadScreenShotImage(@PathVariable String fileName) throws IOException {
        return  ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.screenShotImageStorageServiceImpl.downloadScreenShotImage(fileName)).build());
    }

    @GetMapping("/{studentId}/{examId}")
    public ResponseEntity<SuccessResponseDTO> downloadScreenShotImageList(@PathVariable("studentId") Long studentId ,@PathVariable("examId") Long examId) throws IOException {
        return  ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.screenShotImageStorageServiceImpl.downloadScreenShotImageList(studentId,examId)).build());
    }

    @PostMapping
    public ResponseEntity<SuccessResponseDTO> uploadScreenShotImage(@RequestParam("image")  MultipartFile multipartFile , Long examId , Long studentId ) throws BadRequestException, IOException {
        this.screenShotImageStorageServiceImpl.uploadScreenShotImage(multipartFile ,examId,studentId);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("uploaded").build());
    }


}
