package com.onlineexamportal.controller;

import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.RequestExamDto;

import com.onlineexamportal.dto.RequestResultDto;
import com.onlineexamportal.dto.ResponseExamDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.NotEnoughQuestionsException;
import com.onlineexamportal.services.impl.ExamServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/exam")
@CrossOrigin
(origins = "http://localhost:4200")
public class ExamController {

    @Autowired
    private ExamServiceImpl examServiceImpl;

    @GetMapping("/all")
    public ResponseEntity<SuccessResponseDTO> getExamList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.examServiceImpl.examList()).build());
    }

    @GetMapping("/exam-student-list")
    public ResponseEntity<SuccessResponseDTO> getExamStudentList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.examServiceImpl.examStudentList()).build());
    }

    @GetMapping("/student-exam/{studentId}")
    public ResponseEntity<SuccessResponseDTO> getStudentExam( @PathVariable("studentId") Long studentId ) throws BadRequestException{
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data(this.examServiceImpl.studentExamList(studentId)).build());
    }

    @GetMapping("/remaining-exam-student-list/{examId}")
    public ResponseEntity<SuccessResponseDTO> getRemainingExamStudentList(@PathVariable("examId") Long examId) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.examServiceImpl.remainingExamStudentList(examId)).build());
    }
    @GetMapping("/all/{studentId}")
    public ResponseEntity<SuccessResponseDTO> getExamListForStudent(@PathVariable("studentId") Long studentId) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.examServiceImpl.examListForStudent(studentId)).build());
    }

    @GetMapping("/{examId}")
    public ResponseEntity<SuccessResponseDTO> getExam(@PathVariable("examId") Long examId) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.examServiceImpl.getExam(examId)).build());
    }


    @GetMapping("/my-exam/{userId}")
    public ResponseEntity<SuccessResponseDTO> myExams(@PathVariable("userId") Long userId) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.examServiceImpl.showMyExam(userId)).build());
    }


    @PostMapping("/")
    public ResponseEntity<SuccessResponseDTO> createExam( @RequestBody RequestExamDto requestExamDto) throws BadRequestException, NotEnoughQuestionsException {
        examServiceImpl.createExam(requestExamDto);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("Exam created successfully").build());
    }

    @DeleteMapping("/{examId}")
    public ResponseEntity<SuccessResponseDTO> deleteExam(@PathVariable("examId") Long examId) throws BadRequestException {
        examServiceImpl.deleteExam(examId);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("Exam deleted successfully").build());
    }



    @PutMapping("/{examId}")
    public ResponseEntity<SuccessResponseDTO> updateExam(@PathVariable("examId") Long examId, @RequestBody ResponseExamDto responseExamDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(examServiceImpl.updateExam(examId, responseExamDto)).build());
    }


    @PutMapping("/exam-student/{examId}")
    public ResponseEntity<SuccessResponseDTO> addExamStudent(@PathVariable("examId") Long examId, @RequestBody List<Long>studentIds) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(examServiceImpl.addExamStudent(examId, studentIds)).build());
    }



    @PostMapping("/detail")
    public ResponseEntity<SuccessResponseDTO> getExamDetailForStudent(@RequestBody RequestResultDto requestResultDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.examServiceImpl.getExamDetailForStudent(requestResultDto)).build());
    }
}
