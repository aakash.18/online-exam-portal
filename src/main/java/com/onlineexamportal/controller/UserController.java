package com.onlineexamportal.controller;
import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.LoginRequestDto;
import com.onlineexamportal.dto.ResponseUserDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.UnauthorizedException;
import com.onlineexamportal.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin
(origins = "http://localhost:4200")
public class UserController {
    @Autowired
    private UserServiceImpl userServiceImpl;


    @GetMapping("/all")
    public ResponseEntity<SuccessResponseDTO> getUserList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.userList()).build());
    }
    @GetMapping("/all/students")
    public ResponseEntity<SuccessResponseDTO> getStudentList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.getAllUserStudent() ).build());
    }
    @PostMapping("/login")
    public ResponseEntity<SuccessResponseDTO> login(@RequestBody LoginRequestDto loginRequestDto) throws UnauthorizedException, BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.Login(loginRequestDto)).build());
    }


    @GetMapping("/{UserName}")
    public ResponseEntity<SuccessResponseDTO> getUser(@PathVariable("UserName") String adminUserName) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.getUserByUserName(adminUserName)).build());
    }
    @GetMapping("/{userId}/")
    public ResponseEntity<SuccessResponseDTO> getUserById(@PathVariable("userId") Long userId) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.getUserByUserId(userId)).build());
    }


    @GetMapping("/remaining-exam-student-list/{examId}")
    public ResponseEntity<SuccessResponseDTO> getRemainingExamStudentList(@PathVariable("examId") Long examId) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.remainingExamStudentList(examId)).build());
    }

    @GetMapping("/enrolled-exam-student-list/{examId}")
    public ResponseEntity<SuccessResponseDTO> getEnrolledExamStudentList(@PathVariable("examId") Long examId) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.findEnrolledStudents(examId)).build());
    }

    //TODO (6)
    @DeleteMapping("/remove-student-from-exam/{examId}/{userId}")
    public ResponseEntity<SuccessResponseDTO> removeStudentFromExam(@PathVariable("userId") Long userId,@PathVariable("examId") Long examId) throws BadRequestException {
        userServiceImpl.removeStudentFromExam(examId,userId);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("user (Student) deleted successfully").build());
    }


    @PostMapping("/")
    public ResponseEntity<SuccessResponseDTO> createUser(@Validated @RequestBody ResponseUserDto responseUserDto) throws BadRequestException {
        userServiceImpl.createUser(responseUserDto);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("User created successfully").build());
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<SuccessResponseDTO> deleteUser(@PathVariable("userId") Long userId ) throws BadRequestException {
        userServiceImpl.deleteUser(userId);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("User deleted successfully").build());
    }

    @PutMapping("/{userId}")
    public ResponseEntity<SuccessResponseDTO> updateUser(@PathVariable("userId") Long userId,@RequestBody ResponseUserDto responseUserDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(userServiceImpl.updateUser(userId, responseUserDto)).build());
    }

    @GetMapping("/admin-dashboard")
    public ResponseEntity<SuccessResponseDTO> getAdminDashboard() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(userServiceImpl.getAdminDashboard()).build());
    }

    @GetMapping("/student-dashboard/{studentId}")
    public ResponseEntity<SuccessResponseDTO> getStudentDashboard(@PathVariable("studentId") Long studentId) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(userServiceImpl.getStudentDashboard(studentId)).build());
    }




}
