package com.onlineexamportal.controller;
import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.RequestCategoryDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.services.impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/category")
@CrossOrigin
public class CategoryController {

    @Autowired
    private CategoryServiceImpl categoryServiceImpl;
    @GetMapping("/all")
    public ResponseEntity<SuccessResponseDTO> getCategoryList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200)
                .data(this.categoryServiceImpl.categoryDtoList()).build());
    }

    @PostMapping("/")
    public ResponseEntity<SuccessResponseDTO> createUser(@Validated @RequestBody RequestCategoryDto requestCategoryDto) throws BadRequestException {
        categoryServiceImpl.createCategory(requestCategoryDto);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("User created successfully").build());
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<SuccessResponseDTO> deleteCategory(@PathVariable("categoryId") Long categoryId ) throws BadRequestException {
        categoryServiceImpl.deleteCategory(categoryId);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("Category deleted successfully").build());
    }

    @PutMapping("/{categoryId}")
    public ResponseEntity<SuccessResponseDTO> updateUser(@PathVariable("categoryId") Long categoryId,@RequestBody RequestCategoryDto requestCategoryDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(categoryServiceImpl.updateCategory(categoryId,requestCategoryDto)).build());
    }

}
