package com.onlineexamportal.controller;


import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.dto.RequestSelectedAnswerByCategoryDto;
import com.onlineexamportal.dto.RequestStudentSelectedAnswerDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.services.impl.StudentSelectedAnswerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/selected-answer")
@CrossOrigin
(origins = "http://localhost:4200")
public class StudentSelectedAnswerController {
    @Autowired
    private StudentSelectedAnswerServiceImpl studentSelectedAnswerServiceImpl;

    @GetMapping("/all")
    public ResponseEntity<SuccessResponseDTO> getAnswerList(){
        return  ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.studentSelectedAnswerServiceImpl.questionList()).build());
    }
    @GetMapping("/{studentSelectedAnswerId}")
    public ResponseEntity<SuccessResponseDTO> getQuestion(@PathVariable("studentSelectedAnswerId") Long studentSelectedAnswerId) throws BadRequestException {
        return  ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.studentSelectedAnswerServiceImpl.getAnswer(studentSelectedAnswerId)).build());
    }


    @PostMapping("/")
    public ResponseEntity<SuccessResponseDTO> addAnswer(@RequestBody RequestStudentSelectedAnswerDto requestStudentSelectedAnswerDto) throws BadRequestException {
        studentSelectedAnswerServiceImpl.addAnswer(requestStudentSelectedAnswerDto);
        return  ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("Answer added successfully").build());


    }


    @PutMapping("/{studentSelectedAnswerId}")
    public ResponseEntity<SuccessResponseDTO> updateQuestion(@PathVariable("studentSelectedAnswerId") Long studentSelectedAnswerId, @RequestBody RequestStudentSelectedAnswerDto requestStudentSelectedAnswerDto) throws BadRequestException {

        this.studentSelectedAnswerServiceImpl.updateAnswer(studentSelectedAnswerId,requestStudentSelectedAnswerDto);
        return ResponseEntity.ok(SuccessResponseDTO.builder().data("question updated successfully").status(200).build());

    }

    @PostMapping("/category")
    public ResponseEntity<SuccessResponseDTO> getStudentAnswersByCategory(@RequestBody RequestSelectedAnswerByCategoryDto requestSelectedAnswerByCategoryDto) {
        return ResponseEntity.ok(SuccessResponseDTO.builder()
                .data(this.studentSelectedAnswerServiceImpl.getStudentAnswersByCategory(requestSelectedAnswerByCategoryDto))
                .status(200).build());
    }

}
