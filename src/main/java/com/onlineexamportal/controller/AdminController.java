package com.onlineexamportal.controller;
import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.LoginRequestDto;
import com.onlineexamportal.dto.ResponseUserDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.UnauthorizedException;
import com.onlineexamportal.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/admin")
@CrossOrigin
(origins = "http://localhost:4200")
public class AdminController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @GetMapping("/all")
    public ResponseEntity<SuccessResponseDTO> getAdminList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.userList()).build());
    }


    @PostMapping("/login")
    public ResponseEntity<SuccessResponseDTO> login(@RequestBody LoginRequestDto loginRequestDto) throws UnauthorizedException, BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.Login(loginRequestDto)).build());
    }

    @GetMapping("/{adminUserName}")
    public ResponseEntity<SuccessResponseDTO> getAdmin(@PathVariable("adminUserName") String adminUserName) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.userServiceImpl.getUserByUserName(adminUserName)).build());
    }

    @PostMapping("/")
    public ResponseEntity<SuccessResponseDTO> createAdmin(@Validated @RequestBody ResponseUserDto responseUserDto) throws BadRequestException {
         userServiceImpl.createUser(responseUserDto);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("Admin created successfully").build());
    }

    @DeleteMapping("/{adminId}")
    public ResponseEntity<SuccessResponseDTO> deleteAdmin(@PathVariable("adminId") Long adminId ) throws BadRequestException {
        userServiceImpl.deleteUser(adminId);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("Admin deleted successfully").build());
    }

    @PutMapping("/{adminId}")
    public ResponseEntity<SuccessResponseDTO> updateAdmin(@PathVariable("adminId") Long adminId,@RequestBody ResponseUserDto adminDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(userServiceImpl.updateUser(adminId,adminDto)).build());
    }

    //get current user  - security
    @GetMapping("/current-user")
    public String getLoggedInUser(Principal principal){
        return principal.getName();
    }



}
