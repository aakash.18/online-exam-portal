package com.onlineexamportal.controller;

import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.services.impl.QuestionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/question")
@CrossOrigin
(origins = "http://localhost:4200")
public class QuestionController {

    @Autowired
    private QuestionServiceImpl questionServiceImpl;

    @GetMapping("/all")
    public ResponseEntity<SuccessResponseDTO> getQuestionList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.questionServiceImpl.questionList()).build());
    }

    //pagination and sorting
    @GetMapping("/all-questions")
    public ResponseEntity<SuccessResponseDTO> getAllQuestionList(@RequestParam(value = "pageNumber",defaultValue = "1",required = false)Integer pageNumber ,
                                                                 @RequestParam(value = "pageSize",defaultValue = "5",required = false) Integer pageSize,
                                                                 @RequestParam(value = "sortBy",defaultValue = "questionId",required = false) String sortBy) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.questionServiceImpl.getAllQuestion(pageNumber,pageSize,sortBy)).build());
    }

    //searching
    @GetMapping("/search/")
    public ResponseEntity<SuccessResponseDTO> searchByQuestion(@RequestParam(value = "keyword",defaultValue = "",required = false) String keywords) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.questionServiceImpl.searchQuestions(keywords)).build());
    }


    @GetMapping("/mcq/all")
    public ResponseEntity<SuccessResponseDTO> getMcqQuestionList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.questionServiceImpl.getMcqQuestionList()).build());
    }

    @GetMapping("/programming/all")
    public ResponseEntity<SuccessResponseDTO> getProgrammingQuestionList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.questionServiceImpl.getProgrammingQuestionList()).build());
    }

    @GetMapping("/category-question-count")
    public ResponseEntity<SuccessResponseDTO> getCategoryQuestionCount() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.questionServiceImpl.getCategoryQuestionCount()).build());
    }

    @GetMapping("/{questionId}")
    public ResponseEntity<SuccessResponseDTO> getQuestion(@PathVariable("questionId") Long questionId) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.questionServiceImpl.getQuestion(questionId)).build());
    }

    @PostMapping
    public ResponseEntity<SuccessResponseDTO> createQuestion(@RequestBody QuestionDto questionDto) throws BadRequestException {
//        questionServiceImpl.createQuestion(questionDto);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data(questionServiceImpl.createQuestion(questionDto)).build());
    }

    @DeleteMapping("/{questionId}")
    public ResponseEntity<SuccessResponseDTO> deleteQuestion(@PathVariable("questionId") Long questionId) throws BadRequestException {
        this.questionServiceImpl.deleteQuestion(questionId);
        return ResponseEntity.ok(SuccessResponseDTO.builder().data("question deleted successfully").status(200).build());
    }


    @PutMapping("/{questionId}")
    public ResponseEntity<SuccessResponseDTO> updateQuestion(@PathVariable("questionId") Long questionId, @RequestBody QuestionDto questionDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().data(this.questionServiceImpl.updateQuestion(questionId, questionDto)).status(200).build());

    }
}
