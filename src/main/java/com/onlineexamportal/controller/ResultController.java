package com.onlineexamportal.controller;

import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.FinalResultResponseDto;
import com.onlineexamportal.dto.RequestGenerateExamDto;
import com.onlineexamportal.dto.RequestResultDto;
import com.onlineexamportal.dto.UpdateResultStatusDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.services.impl.ResultServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/result")
@CrossOrigin
(origins = "http://localhost:4200")
public class ResultController {


    @Autowired
    private ResultServiceImpl resultServiceImpl;

    @GetMapping("/all")
    public ResponseEntity<SuccessResponseDTO> getResultList() {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.resultServiceImpl.resultList()).build());
    }

    @GetMapping("/all/{userId}")
    public ResponseEntity<SuccessResponseDTO> getResultList(@PathVariable("userId") Long userId) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.resultServiceImpl.getResultsByStudentId(userId)).build());
    }

    @GetMapping("/result")
    public ResponseEntity<SuccessResponseDTO> getFinalResult(@RequestBody FinalResultResponseDto finalResultResponseDto) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.resultServiceImpl.getFinalResult(finalResultResponseDto)).build());
    }


    @PostMapping("/generate-result")
    public ResponseEntity<SuccessResponseDTO> generateResult(@RequestBody RequestGenerateExamDto requestGenerateExamDto) {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.resultServiceImpl.generateResult(requestGenerateExamDto)).build());
    }

    @PostMapping("/status")
    public ResponseEntity<SuccessResponseDTO> getResultByStatus(@RequestBody UpdateResultStatusDto updateResultStatusDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.resultServiceImpl.getResultByStatus(updateResultStatusDto)).build());
    }

    @PostMapping("/result-status")
    public ResponseEntity<SuccessResponseDTO> getResultByResultStatusList(@RequestBody UpdateResultStatusDto updateResultStatusDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.resultServiceImpl.getResultByResultStatus(updateResultStatusDto)).build());
    }

    @GetMapping("/{resultId}")
    public ResponseEntity<SuccessResponseDTO> getResult(@PathVariable("resultId") Long resultId) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(this.resultServiceImpl.getResultByResultId(resultId)).build());
    }

    @PostMapping("/create-result")
    public ResponseEntity<SuccessResponseDTO> createResult(@RequestBody RequestResultDto requestResultDto) throws BadRequestException {
        resultServiceImpl.createResult(requestResultDto);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("Result created successfully").build());
    }

    @DeleteMapping("/delete-result/{resultId}")
    public ResponseEntity<SuccessResponseDTO> deleteResult(@PathVariable("resultId") Long resultId) throws BadRequestException {
        this.resultServiceImpl.deleteResult(resultId);
        return ResponseEntity.ok(SuccessResponseDTO.builder().data(resultServiceImpl.getResultByResultId(resultId)).status(200).build());
    }

    @PutMapping("/update-result/{resultId}")
    public ResponseEntity<SuccessResponseDTO> updateAdmin(@PathVariable("resultId") Long resultId, @RequestBody RequestResultDto requestResultDto) throws BadRequestException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(200).data(resultServiceImpl.updateResult(resultId, requestResultDto)).build());
    }
}
