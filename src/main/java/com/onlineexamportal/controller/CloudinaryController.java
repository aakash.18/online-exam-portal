package com.onlineexamportal.controller;

import com.cloudinary.Cloudinary;
import com.onlineexamportal.dto.ApiResponse.SuccessResponseDTO;
import com.onlineexamportal.dto.RequestQuestionImageDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.services.CloudinaryImageService;
import com.onlineexamportal.services.impl.CloudinaryImageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/cloudinary/upload")
@CrossOrigin(origins = "http://localhost:4200")
public class CloudinaryController {

    @Autowired
    private CloudinaryImageServiceImpl cloudinaryImageServiceImpl;
//    @PostMapping("/")
//    public ResponseEntity<SuccessResponseDTO> uploadImage(@RequestParam("image") MultipartFile image) throws BadRequestException, IOException {
//        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data(this.cloudinaryImageServiceImpl.upload(image)).build());
//    }

    @PostMapping("/")
    public ResponseEntity<SuccessResponseDTO> uploadImage(@RequestParam("image") MultipartFile image) throws BadRequestException, IOException {
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data(this.cloudinaryImageServiceImpl.upload(image)).build());
    }

    @PutMapping("/multiple-images/{questionId}")
    public ResponseEntity<SuccessResponseDTO> uploadMultipleImage(
            @PathVariable("questionId") Long questionId ,
            @RequestParam (required=false) MultipartFile imageUrl ,
            @RequestParam (required=false) MultipartFile opt1ImageUrl,
            @RequestParam (required=false) MultipartFile opt2ImageUrl,
            @RequestParam (required=false) MultipartFile opt3ImageUrl,
            @RequestParam (required=false) MultipartFile opt4ImageUrl) throws BadRequestException, IOException {

        RequestQuestionImageDto requestQuestionImageDto = new RequestQuestionImageDto();
        requestQuestionImageDto.setImageUrl(imageUrl);
        requestQuestionImageDto.setOpt1ImageUrl(opt1ImageUrl);
        requestQuestionImageDto.setOpt2ImageUrl(opt2ImageUrl);
        requestQuestionImageDto.setOpt3ImageUrl(opt3ImageUrl);
        requestQuestionImageDto.setOpt4ImageUrl(opt4ImageUrl);
        this.cloudinaryImageServiceImpl.uploadImages(questionId,requestQuestionImageDto);
        return ResponseEntity.ok(SuccessResponseDTO.builder().status(201).data("Success").build());
    }

}
