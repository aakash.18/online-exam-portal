package com.onlineexamportal.entities;


import lombok.*;



import javax.persistence.*;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Result {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long resultId;
    private float logicalMarks;
    private float technicalMarks;
    private float programmingMarks;

    private boolean logicalStatus;
    private boolean technicalStatus;
    private boolean programmingStatus;


    private boolean status;

    private boolean isResultFinal;

    @ManyToOne
    private  User user;
    @ManyToOne
    private Exam exam;



}
