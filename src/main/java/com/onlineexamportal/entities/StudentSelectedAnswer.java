package com.onlineexamportal.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentSelectedAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long studentSelectedAnswerId;
    private String selectedAnswer;
    @ManyToOne
    private Exam exam;
    @ManyToOne
    private User user;
    @ManyToOne
    private Question question;

    private int mark;
}
