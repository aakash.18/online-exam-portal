package com.onlineexamportal.entities;
import com.onlineexamportal.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;
    private String userName;
    private String password;
    private String name;
    private boolean isActive;
    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @OneToMany(mappedBy = "user")
    private List<Result> resultList;

    @OneToMany(mappedBy = "user")
    private List<Question>questionList;

    @OneToMany(mappedBy = "user")
    private List<Exam>examList;


    @ManyToMany
    @JoinTable(
            name = "exam_student",
            inverseJoinColumns = @JoinColumn(name = "exam_id"),
            joinColumns  = @JoinColumn(name = "user_id"))
    private List<Exam> examStudentList;

}
