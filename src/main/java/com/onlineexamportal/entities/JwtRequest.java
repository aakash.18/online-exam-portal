package com.onlineexamportal.entities;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class JwtRequest {

    private String userName;
    private String password;


}
