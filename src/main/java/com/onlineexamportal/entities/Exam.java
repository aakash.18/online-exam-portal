package com.onlineexamportal.entities;


import com.onlineexamportal.enums.Difficulty;
import lombok.*;


import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long examId;
    private String examName;
    private int noOfLogicalQuestion;
    private int noOfTechnicalQuestion;
    private int noOfProgrammingQuestion;
    private int logicalPassingMarks;
    private int technicalPassingMarks;
    private int programmingPassingMarks;
    @Enumerated(EnumType.STRING)
    private Difficulty logicalQuestionDifficulty;
    @Enumerated(EnumType.STRING)
    private Difficulty technicalQuestionDifficulty;
    @Enumerated(EnumType.STRING)
    private Difficulty programmingQuestionDifficulty;
    private Long duration;
    private Long startTime;
    private Long endTime;
    private Date date;

    private int totalMarks;

    private Long startDate;
    private Long endDate;

    @ManyToMany
    @JoinTable(
            name = "exam_question",
            joinColumns = @JoinColumn(name = "exam_id"),
            inverseJoinColumns = @JoinColumn(name = "question_id"))
    private List<Question> examQuestionList;


    @ManyToMany
    @JoinTable(
            name = "exam_student",
            joinColumns = @JoinColumn(name = "exam_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> examStudentList;



    @OneToMany(mappedBy = "exam")
    private List<Result>resultList;
    
    @ManyToOne
    private User user;

}
