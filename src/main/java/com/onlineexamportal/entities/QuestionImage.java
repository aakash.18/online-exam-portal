package com.onlineexamportal.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionImage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long questionImageId;

    private String imageUrl;
    private String opt1ImageUrl;
    private String opt2ImageUrl;
    private String opt3ImageUrl;
    private String opt4ImageUrl;

    @OneToOne
    private Question question;
}
