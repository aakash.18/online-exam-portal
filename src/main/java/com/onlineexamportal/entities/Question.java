package com.onlineexamportal.entities;

import com.onlineexamportal.enums.Difficulty;
import lombok.*;
import org.hibernate.annotations.Cascade;


import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long questionId;

    private String question;
    private String opt1;
    private String opt2;
    private String opt3;
    private String opt4;
    @Column(length=512)
    private String correctAns;
    private int mark;
    @Enumerated(EnumType.STRING)
    private Difficulty difficulty;
    private String imgUrls;
    @ManyToOne
    private User user;
    @ManyToOne
    private Category category;
    private boolean isActive;


    @OneToOne(cascade = CascadeType.ALL)
    private QuestionImage questionImage;
}
