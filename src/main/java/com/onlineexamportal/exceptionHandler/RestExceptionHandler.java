package com.onlineexamportal.exceptionHandler;

import com.onlineexamportal.dto.ApiResponse.ErrorResponseDTO;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.UnauthorizedException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler {
    @ExceptionHandler({BadRequestException.class})
    private ResponseEntity<ErrorResponseDTO> handleBadRequestException(HttpServletRequest request,Exception exception){
        ErrorResponseDTO errorResponseDTO = ErrorResponseDTO.builder()
                .message(exception.getMessage())
                .path(request.getRequestURI())
                .status(400).build();
        return  new ResponseEntity<>(errorResponseDTO, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({UnauthorizedException.class})
    private ResponseEntity<ErrorResponseDTO> handleUnauthorizedException(HttpServletRequest request,Exception exception){
        ErrorResponseDTO errorResponseDTO = ErrorResponseDTO.builder()
                .message(exception.getMessage())
                .path(request.getRequestURI())
                .status(401).build();
        return  new ResponseEntity<>(errorResponseDTO, HttpStatus.UNAUTHORIZED);
    }
}
