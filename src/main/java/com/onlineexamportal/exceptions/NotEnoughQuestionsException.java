package com.onlineexamportal.exceptions;

public class NotEnoughQuestionsException extends Exception{
    public NotEnoughQuestionsException(String message) {super(message);}
}
