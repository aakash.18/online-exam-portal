package com.onlineexamportal.exceptions;

public class BadRequestException extends Exception {
    public BadRequestException(String message) {super(message);}
}
