package com.onlineexamportal.exceptions;

public class MinMarksAlreadySetException extends Exception{
    public MinMarksAlreadySetException(String message) {super(message);}
}
