package com.onlineexamportal.services;

import com.onlineexamportal.dto.RequestSelectedAnswerByCategoryDto;
import com.onlineexamportal.dto.RequestStudentSelectedAnswerDto;
import com.onlineexamportal.dto.ResponseSelectedAnswerByCategoryDto;
import com.onlineexamportal.dto.ResponseStudentSelectedAnswerDto;
import com.onlineexamportal.exceptions.BadRequestException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentSelectedAnswerService {
    public void addAnswer(RequestStudentSelectedAnswerDto requestStudentSelectedAnswerDto) throws BadRequestException;
    public RequestStudentSelectedAnswerDto updateAnswer(Long studentSelectedAnswerId, RequestStudentSelectedAnswerDto responseStudentSelectedAnswerDto) throws BadRequestException;
    public ResponseStudentSelectedAnswerDto getAnswer(Long studentSelectedAnswerId) throws BadRequestException;
    public List<ResponseSelectedAnswerByCategoryDto> getStudentAnswersByCategory(RequestSelectedAnswerByCategoryDto requestSelectedAnswerByCategoryDto );
}
