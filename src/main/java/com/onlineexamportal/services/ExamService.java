package com.onlineexamportal.services;
import com.onlineexamportal.dto.QuestionToSelectedAnswerDto;
import com.onlineexamportal.dto.RequestExamDto;
import com.onlineexamportal.dto.RequestResultDto;
import com.onlineexamportal.dto.ResponseExamDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.NotEnoughQuestionsException;

import java.util.List;

public interface ExamService {

    public void createExam(RequestExamDto requestExamDto) throws BadRequestException, NotEnoughQuestionsException;
    public String deleteExam(Long examId) throws BadRequestException;
    public ResponseExamDto updateExam(Long examId, ResponseExamDto responseExamDto) throws BadRequestException;
    public ResponseExamDto getExam(Long examId) throws BadRequestException;
    public List<ResponseExamDto>  showMyExam(Long adminId) throws BadRequestException;
    public List<ResponseExamDto> examList();

    public List<ResponseExamDto> examListForStudent(Long studentId);

    public List<QuestionToSelectedAnswerDto> getExamDetailForStudent(RequestResultDto requestResultDto) throws BadRequestException;
}
