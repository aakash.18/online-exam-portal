
package com.onlineexamportal.services;

import com.onlineexamportal.dto.LoginRequestDto;
import com.onlineexamportal.dto.RoleDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.UnauthorizedException;

public interface RoleService {

    public void createRole(RoleDto roleDto) throws BadRequestException;
    public RoleDto Login(LoginRequestDto loginRequestDto) throws BadRequestException, UnauthorizedException;
}
