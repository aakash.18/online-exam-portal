package com.onlineexamportal.services;

import com.onlineexamportal.dto.RequestCategoryPassingMarksDto;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.MinMarksAlreadySetException;

public interface CategoryPassingMarksService {

    public void Add(RequestCategoryPassingMarksDto requestCategoryPassingMarksDto) throws BadRequestException, MinMarksAlreadySetException;
    public String removeMinMarks(Long categoryPassingMarksId) throws BadRequestException;
    public RequestCategoryPassingMarksDto updateMinMarks(Long categoryPassingMarksId,RequestCategoryPassingMarksDto requestCategoryPassingMarksDto) throws BadRequestException;
    public RequestCategoryPassingMarksDto getCategoryPassingMarks(String categoryName) throws BadRequestException;
}
