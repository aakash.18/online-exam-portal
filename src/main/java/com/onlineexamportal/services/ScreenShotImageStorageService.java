package com.onlineexamportal.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ScreenShotImageStorageService {


    String uploadScreenShotImage(MultipartFile file, Long examId, Long studentId) throws IOException;

    public byte[] downloadScreenShotImage(String fileName) throws IOException;
    public List<byte[]> downloadScreenShotImageList(Long examId , Long StudentId) throws IOException;
}
