package com.onlineexamportal.services;

import com.onlineexamportal.dto.RequestCategoryDto;
import com.onlineexamportal.dto.ResponseCategoryDto;
import com.onlineexamportal.exceptions.BadRequestException;

public interface CategoryService {

    public void createCategory(RequestCategoryDto requestCategoryDto) throws BadRequestException;
    public String deleteCategory(Long categoryId) throws BadRequestException;
    public RequestCategoryDto updateCategory(Long categoryId, RequestCategoryDto requestCategoryDto) throws BadRequestException;
    public ResponseCategoryDto getByCategoryName(String categoryName) throws BadRequestException;

}
