package com.onlineexamportal.services.impl;
import com.onlineexamportal.Mapper.ExamMapper;
import com.onlineexamportal.Mapper.UserMapper;
import com.onlineexamportal.dto.*;
import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.User;
import com.onlineexamportal.enums.UserRole;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.UnauthorizedException;
import com.onlineexamportal.repositories.*;
import com.onlineexamportal.services.ResultService;
import com.onlineexamportal.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
@CrossOrigin
public class UserServiceImpl implements UserService {
    private final UserMapper userMapper;
    private final ExamMapper examMapper;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExamRepository examRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ResultRepository resultRepository;
    @Autowired
    private ResultService resultService;

    @Autowired
    private ExamStudentRepository examStudentRepository;


    public List<ResponseUserDto> userList()  {
        return this.userRepository.
                findAll().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void createUser(ResponseUserDto responseUserDto) throws BadRequestException {
        Optional<User> optionalUser = this.userRepository.getByUserName(responseUserDto.getUserName());
        if(optionalUser.isPresent()){
            throw new BadRequestException("Username exists , try another!");
        }

        User newUser = new User();
        newUser.setUserName(responseUserDto.getUserName());
        newUser.setPassword(responseUserDto.getPassword());
        newUser.setUserRole(responseUserDto.getUserRole());
        newUser.setActive(true);
        newUser.setName(responseUserDto.getName());
        userRepository.save(newUser);
    }


    @Override
    public String deleteUser(Long userId) throws BadRequestException {
        Optional<User> optionalUser = this.userRepository.findById(userId);
        if( !optionalUser.isPresent()){
            throw new BadRequestException("Admin does not exists");
        }
        optionalUser.get().setActive(false);
        return "User deleted successfully";
    }


    //TODO (4)
    public List<ResponseUserDto> remainingExamStudentList( Long examId){
        return this.userRepository.findRemainingStudents(examId).stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }


    //TODO (5)
    public List<ResponseUserDto> findEnrolledStudents( Long examId){
        return this.userRepository.findEnrolledStudents(examId).stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    //TODO (6)

    public String removeStudentFromExam(Long examId ,Long userId) throws BadRequestException {
        Optional<User> optionalUser = this.userRepository.findById(userId);
        if( !optionalUser.isPresent()){
            throw new BadRequestException("user (Student) does not exists");
        }
        this.userRepository.removeStudentFromExam(examId,userId);
        return "User(Student) removed successfully";
    }

    @Override
    public ResponseUserDto updateUser(Long userId, ResponseUserDto responseUserDto) throws BadRequestException {
        Optional<User> optionalUser = this.userRepository.findById(userId);
        if( !optionalUser.isPresent()){
            throw new BadRequestException("User does not exists");
        }

        User user = optionalUser.get();
        user.setUserName(responseUserDto.getUserName());
//        user.setPassword(responseUserDto.getPassword());
//        user.setUserRole(responseUserDto.getUserRole());
        user.setName(responseUserDto.getName());
        userRepository.save(user);
        return userMapper.toDto(user);
    }

    @Override
    public ResponseUserDto getUserByUserName(String userName) throws BadRequestException {
        Optional<User> user = this.userRepository.getByUserName(userName);
        if( user == null){
            throw new BadRequestException("Admin does not exists");
        }
        return userMapper.toDto(this.userRepository.getByUserName(userName).get());
    }

    @Override
    public ResponseUserDto getUserByUserId(Long userId) throws BadRequestException {
        User user = this.userRepository.findByUserIdAndIsActive(userId, true);
        if(user == null){
            throw new BadRequestException("USER does not exists");
        }
        return userMapper.toDto(this.userRepository.findByUserIdAndIsActive(userId,true));
    }

    @Override
    public List<ResponseUserStudentDto> getAllUserStudent() {
        return this.userRepository.
                findAllByUserRoleAndIsActive(UserRole.STUDENT,true).stream()
                .map(userMapper::toResponseUserStudentDto).collect(Collectors.toList());
    }

    @Override
    public ResponseUserDto Login(LoginRequestDto loginRequestDto) throws BadRequestException, UnauthorizedException {
        Optional<User> optionalUser =this.userRepository.getByUserName(loginRequestDto.getUserName());

        if(!optionalUser.isPresent()) {
            throw new UnauthorizedException("username does not exists");
        }
        if(optionalUser.get().getPassword().trim().equals(loginRequestDto.getPassword().trim()) && optionalUser.get().getUserRole().toString().equals(loginRequestDto.getUserRole())){


            return  userMapper.toDto(optionalUser.get());
        }
        throw new UnauthorizedException("Invalid password");
    }

    @Override
    public void disableStudent(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        optionalUser.ifPresent(user -> user.setActive(false));
    }

    @Override
    public AdminDashboardDto getAdminDashboard() {
        AdminDashboardDto adminDashboardDto = new AdminDashboardDto();
        adminDashboardDto.setTotalExams(examRepository.findAll().size());
        adminDashboardDto.setTotalQuestions(questionRepository.findAllByIsActive(true).size());
        adminDashboardDto.setTotalStudents(userRepository.findAllByIsActive(true).size());
        adminDashboardDto.setTotalLogicalQuestions(questionRepository.findAllByIsActiveAndCategory_CategoryId(true, 4L).size());
        adminDashboardDto.setTotalProgrammingQuestions(questionRepository.findAllByIsActiveAndCategory_CategoryId(true, 5L).size());
        adminDashboardDto.setTotalTechnicalQuestions(questionRepository.findAllByIsActiveAndCategory_CategoryId(true,3L).size());
        return adminDashboardDto;
    }

    @Override
    public StudentDashboardDto getStudentDashboard(Long studentId) {
        StudentDashboardDto studentDashboardDto = new StudentDashboardDto();
        studentDashboardDto.setTotalGivenExams(resultRepository.findAllByUser_UserId(studentId).size());
        studentDashboardDto.setTotalPassedExams(resultRepository.findAllByUser_UserIdAndIsResultFinalAndStatus(studentId, true, true).size());
        studentDashboardDto.setTotalFailedExams(resultRepository.findAllByUser_UserIdAndIsResultFinalAndStatus(studentId, true, false).size());
        List<ResponseResultDetailDto> responseResultDetailDtoList = resultService.getResultsByStudentId(studentId);
        if (responseResultDetailDtoList.size() > 10) {
         responseResultDetailDtoList = responseResultDetailDtoList.subList(0, 11);
        }
        studentDashboardDto.setResponseResultDetailDtoList(responseResultDetailDtoList);
        return studentDashboardDto;
    }

}
