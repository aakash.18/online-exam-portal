package com.onlineexamportal.services.impl;

import com.onlineexamportal.Mapper.CategoryMapper;
import com.onlineexamportal.dto.RequestCategoryDto;
import com.onlineexamportal.dto.ResponseCategoryDto;
import com.onlineexamportal.entities.Category;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.repositories.CategoryRepository;
import com.onlineexamportal.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Service
@Transactional
@RequiredArgsConstructor
@CrossOrigin
public class CategoryServiceImpl implements CategoryService {
    private final CategoryMapper categoryMapper;
    @Autowired
    private CategoryRepository categoryRepository;

    public List<ResponseCategoryDto> categoryDtoList()  {
        return this.categoryRepository.
                findAll().stream()
                .map(categoryMapper::toResponseCategoryDto)
                .collect(Collectors.toList());
    }
    @Override
    public void createCategory(RequestCategoryDto requestCategoryDto) throws BadRequestException {

        Category category = this.categoryRepository.getByCategoryName(requestCategoryDto.getCategoryName());
        if(category != null){
            throw new BadRequestException("Category exists , try another!");
        }

        Category newCategory = new Category();
        newCategory.setCategoryName(requestCategoryDto.getCategoryName());
        categoryRepository.save(newCategory);

    }

    @Override
    public String deleteCategory(Long categoryId) throws BadRequestException {
        Optional<Category> optionalCategory = this.categoryRepository.findById(categoryId);
        if( !optionalCategory.isPresent()){
            throw new BadRequestException("Admin does not exists");
        }
        this.categoryRepository.deleteById( categoryId);
        return "User deleted successfully";
    }

    @Override
    public RequestCategoryDto updateCategory(Long categoryId, RequestCategoryDto requestCategoryDto) throws BadRequestException {
        Optional<Category> optionalCategory = this.categoryRepository.findById(categoryId);
        System.out.println(optionalCategory.get().getCategoryName());
        if( !optionalCategory.isPresent()){
            throw new BadRequestException("User does not exists");
        }

        Category category = optionalCategory.get();
        category.setCategoryName(requestCategoryDto.getCategoryName());
        System.out.println(optionalCategory.get().getCategoryName());
        categoryRepository.save(category);
        return categoryMapper.toRequestCategoryDto(category);
    }

    @Override
    public ResponseCategoryDto getByCategoryName(String categoryName) throws BadRequestException {
        return null;
    }
}
