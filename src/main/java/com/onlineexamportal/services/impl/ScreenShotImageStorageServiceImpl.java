package com.onlineexamportal.services.impl;

import com.onlineexamportal.entities.QuestionImage;
import com.onlineexamportal.entities.ScreenShotImages;
import com.onlineexamportal.repositories.ScreenShotImagesRepository;
import com.onlineexamportal.services.ScreenShotImageStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.nio.file.Files.readAllBytes;

@Service
@Transactional
@RequiredArgsConstructor
public class ScreenShotImageStorageServiceImpl implements ScreenShotImageStorageService {

    @Autowired
    private ScreenShotImagesRepository screenShotImagesRepository;

    private final String FOLDER_PATH = "C:/ROIMA/ONLINE EXAM PORTAL/spring boot/New folder (2)/OnlineExamPortal/src/main/java/com/onlineexamportal/screenShotImages/";private final String QUESTION_IMAGE_PATH = "C:/ROIMA/ONLINE EXAM PORTAL/spring boot/New folder (2)/OnlineExamPortal/src/main/java/com/onlineexamportal/questionImages/";


    @Override
    public String uploadScreenShotImage(MultipartFile file, Long examId, Long studentId) throws IOException {
        String filePath = FOLDER_PATH + file.getOriginalFilename();
        ScreenShotImages screenShotImages = screenShotImagesRepository.save(
                ScreenShotImages.builder()
                        .name(file.getOriginalFilename())
                        .type(file.getContentType())
                        .fiePath(filePath)
                        .examId(examId)
                        .studentId(studentId)
                        .build());
        file.transferTo(new File(filePath));
        return "file uploaded successfully: " + filePath;
    }

    @Override
    public byte[] downloadScreenShotImage(String fileName) throws IOException {
        Optional<ScreenShotImages> screenShotImages = screenShotImagesRepository.findByName(fileName);
        String filePath = screenShotImages.get().getFiePath();
        byte[] screenShotImage = readAllBytes(new File(filePath).toPath());
        return screenShotImage;
    }

    @Override
    public List<byte[]> downloadScreenShotImageList(Long studentId, Long examId) throws IOException {

        List<byte[]>responseScreenShotImageList = new ArrayList<>() ;
        List<ScreenShotImages>  screenShotImageList = screenShotImagesRepository.findAllByStudentIdAndExamId(studentId,examId);

        System.out.println("size of screenShot image : "+screenShotImageList.size());
        screenShotImageList.forEach(ss -> {
            byte[] screenShotImage;
            String filePath = ss.getFiePath();
            try {
               screenShotImage = readAllBytes(new File(filePath).toPath());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            responseScreenShotImageList.add(screenShotImage);
        });

        return responseScreenShotImageList;
    }

}
