package com.onlineexamportal.services.impl;

import com.onlineexamportal.Mapper.StudentSelectedAnswerMapper;
import com.onlineexamportal.dto.*;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.StudentSelectedAnswer;
import com.onlineexamportal.entities.User;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.repositories.ExamRepository;
import com.onlineexamportal.repositories.QuestionRepository;
import com.onlineexamportal.repositories.StudentSelectedAnswerRepository;
import com.onlineexamportal.repositories.UserRepository;
import com.onlineexamportal.services.StudentSelectedAnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class StudentSelectedAnswerServiceImpl implements StudentSelectedAnswerService {

    private final StudentSelectedAnswerMapper studentSelectedAnswerMapper;

    @Autowired
    private StudentSelectedAnswerRepository studentSelectedAnswerRepository;
    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ExamRepository examRepository;

    @Autowired
    private UserRepository userRepository;

    public List<ResponseStudentSelectedAnswerDto> questionList(){
        return this.studentSelectedAnswerRepository.
                findAll().stream()
                .map(studentSelectedAnswerMapper::toResponseStudentSelectedAnswerDto)
                .collect(Collectors.toList());
    }
    @Override
    public void addAnswer(RequestStudentSelectedAnswerDto requestStudentSelectedAnswerDto) throws BadRequestException {
        Optional<User> optionalUser = this.userRepository.findById(requestStudentSelectedAnswerDto.getUserId());
        if(!optionalUser.isPresent()){
            throw new BadRequestException("Student does not exists");
        }
        StudentSelectedAnswer studentSelectedAnswer = studentSelectedAnswerRepository.findByExam_ExamIdAndUser_UserIdAndQuestion_QuestionId(requestStudentSelectedAnswerDto.getExamId(), requestStudentSelectedAnswerDto.getUserId(), requestStudentSelectedAnswerDto.getQuestionId());
        if (studentSelectedAnswer != null) {
            if(!studentSelectedAnswer.getQuestion().getCategory().getCategoryName().equalsIgnoreCase("programming")) {
                studentSelectedAnswer.setMark(studentSelectedAnswer.getQuestion().getCorrectAns().equalsIgnoreCase(requestStudentSelectedAnswerDto.getSelectedAnswer()) ? 1 : 0);
            }
            studentSelectedAnswer.setSelectedAnswer(requestStudentSelectedAnswerDto.getSelectedAnswer());
            studentSelectedAnswerRepository.save(studentSelectedAnswer);
            return;
        }

    
        Question question = questionRepository.findById(requestStudentSelectedAnswerDto.getQuestionId()).get();
        StudentSelectedAnswer  newAnswer = new StudentSelectedAnswer();
        newAnswer.setUser(optionalUser.get());
        newAnswer.setSelectedAnswer(requestStudentSelectedAnswerDto.getSelectedAnswer());
        newAnswer.setQuestion(question);
        if(!question.getCategory().getCategoryName().equalsIgnoreCase("programming")) {
            newAnswer.setMark(question.getCorrectAns().equalsIgnoreCase(requestStudentSelectedAnswerDto.getSelectedAnswer()) ? 1 : 0);
        } else {
            newAnswer.setMark(0);
        }
        newAnswer.setExam(examRepository.findById(requestStudentSelectedAnswerDto.getExamId()).get());
        studentSelectedAnswerRepository.save(newAnswer);
    }
    @Override
    public List<ResponseSelectedAnswerByCategoryDto> getStudentAnswersByCategory(RequestSelectedAnswerByCategoryDto requestSelectedAnswerByCategoryDto ){
        List<StudentSelectedAnswer> studentSelectedAnswers = studentSelectedAnswerRepository
                .findAllByExam_ExamIdAndUser_UserIdAndQuestion_Category_CategoryId(
                        requestSelectedAnswerByCategoryDto.getExamId(),
                        requestSelectedAnswerByCategoryDto.getUserId(),
                        requestSelectedAnswerByCategoryDto.getCategoryId()
                );

        return studentSelectedAnswers
                .stream()
                .map(studentSelectedAnswerMapper::toResponseSelectedAnswerByCategoryDto)
                .collect(Collectors.toList());
    }

    @Override
    public RequestStudentSelectedAnswerDto updateAnswer(Long studentSelectedAnswerId, RequestStudentSelectedAnswerDto requestStudentSelectedAnswerDto) throws BadRequestException {
        Optional<StudentSelectedAnswer> optionalselectedAnswer = studentSelectedAnswerRepository.findById(studentSelectedAnswerId);

        if(!optionalselectedAnswer.isPresent()){
            throw new BadRequestException("This question does not exists");
        }

        StudentSelectedAnswer updatedAnswer = new StudentSelectedAnswer();

        updatedAnswer.setUser(optionalselectedAnswer.get().getUser());
        updatedAnswer.setStudentSelectedAnswerId(studentSelectedAnswerId);
        updatedAnswer.setSelectedAnswer(requestStudentSelectedAnswerDto.getSelectedAnswer());
        updatedAnswer.setQuestion(questionRepository.findById(requestStudentSelectedAnswerDto.getQuestionId()).get());
        updatedAnswer.setExam(examRepository.findById(requestStudentSelectedAnswerDto.getExamId()).get());


        StudentSelectedAnswer updatedAnswers = studentSelectedAnswerRepository.save(updatedAnswer);
        return studentSelectedAnswerMapper.toRequestStudentSelectedAnswerDto(updatedAnswers);
    }

    @Override
    public ResponseStudentSelectedAnswerDto getAnswer(Long studentSelectedAnswerId) throws BadRequestException {
        if(!this.studentSelectedAnswerRepository.findById(studentSelectedAnswerId).isPresent()){
            throw new BadRequestException("This question does not exists!");
        }
        return studentSelectedAnswerMapper.toResponseStudentSelectedAnswerDto(
                this.studentSelectedAnswerRepository.getReferenceById(studentSelectedAnswerId));
    }


}




