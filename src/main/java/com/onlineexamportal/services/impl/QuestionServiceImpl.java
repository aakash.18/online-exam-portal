package com.onlineexamportal.services.impl;

import com.onlineexamportal.Mapper.QuestionMapper;
import com.onlineexamportal.Mapper.UserMapper;
import com.onlineexamportal.dto.CategoryQuestionCountDto;
import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.dto.ResponseQuestionDto;
import com.onlineexamportal.entities.Category;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.QuestionImage;
import com.onlineexamportal.entities.User;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.repositories.CategoryRepository;
import com.onlineexamportal.repositories.QuestionRepository;
import com.onlineexamportal.repositories.UserRepository;
import com.onlineexamportal.services.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    final QuestionMapper questionMapper;
    final UserMapper userMapper;

    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CategoryRepository categoryRepository;


    @Override
    public List<ResponseQuestionDto> questionList(){
        return this.questionRepository.findAllByIsActive(true).stream()
                .map(questionMapper::toResponseQuestionDto)
                .collect(Collectors.toList());
    }


    // pagination and searching
    public List<ResponseQuestionDto> getAllQuestion(Integer pageNumber , Integer pageSize, String sortBy){

        Pageable p = PageRequest.of(pageNumber,pageSize, Sort.by(sortBy));
        Page<Question> pageQuestion = this.questionRepository.findAllByIsActive(true,p);

        List <Question> allQuestions  = pageQuestion.getContent();
        List<ResponseQuestionDto> questionDtoList = allQuestions.stream()
                .map(questionMapper::toResponseQuestionDto)
                .collect(Collectors.toList());
        return questionDtoList;

    }

    // search Question
    public List<ResponseQuestionDto> searchQuestions(String keyword){

        List<Question>questionList =   this.questionRepository.findByQuestionContainingIgnoreCase(keyword);

        List<ResponseQuestionDto>questionDtoList = questionList.stream().map(questionMapper::toResponseQuestionDto)
                .collect(Collectors.toList());
        return questionDtoList;
    }
    @Override
    public List<ResponseQuestionDto> getMcqQuestionList(){
        return this.questionRepository.
                findByCategory_CategoryNameOrCategory_CategoryName("LOGICAL","TECHNICAL").stream()
                .map(questionMapper::toResponseQuestionDto)
                .collect(Collectors.toList());
    }
    @Override
    public List<ResponseQuestionDto> getProgrammingQuestionList(){
        return this.questionRepository.
                findByCategory_CategoryName("PROGRAMMING").stream()
                .map(questionMapper::toResponseQuestionDto)
                .collect(Collectors.toList());
    }
    @Override
    public ResponseQuestionDto createQuestion(QuestionDto questionDto) throws BadRequestException {
        Optional<User> optionalUser = this.userRepository.findById(questionDto.getAdminId());
        if(!optionalUser.isPresent()){
            throw new BadRequestException("User does not exists");
        }

        Question newQuestion = new Question();
        newQuestion.setQuestion(questionDto.getQuestion());
        newQuestion.setOpt1(questionDto.getOpt1());
        newQuestion.setOpt2(questionDto.getOpt2());
        newQuestion.setActive(true); //TODO : ADDED THIS
        newQuestion.setOpt3(questionDto.getOpt3());
        newQuestion.setOpt4(questionDto.getOpt4());
        Optional<Category> optionalCategory = categoryRepository.findById(questionDto.getTypeOfCategory());
        if(!optionalCategory.isPresent()) throw new BadRequestException("Category does no exists");
        newQuestion.setCategory(optionalCategory.get());
        System.out.println("category Name: "+ optionalCategory.get());
        newQuestion.setMark(questionDto.getMark());
        newQuestion.setCorrectAns(questionDto.getCorrectAns());
        newQuestion.setDifficulty(questionDto.getDifficulty());
        newQuestion.setImgUrls(questionDto.getImgUrls());
        newQuestion.setUser(optionalUser.get());
        System.out.println("admin id: "+optionalUser.get());
        newQuestion.setMark(questionDto.getMark());
        questionRepository.save(newQuestion);
        return  questionMapper.toResponseQuestionDto(newQuestion);
    }

    @Override
    public String deleteQuestion(Long Qid) throws BadRequestException {
        Optional<Question> optionalQuestion = this.questionRepository.findById(Qid);
        if(!optionalQuestion.isPresent()){
            throw new BadRequestException("Question does not exists");
        }
        //TODO: MADE CHANGES HERE
        Question deleteQuestion = optionalQuestion.get();
        deleteQuestion.setActive(false);
        Question updatedQuestion = questionRepository.save(deleteQuestion);
        return "Question deleted successfully";
    }

    @Override
    public ResponseQuestionDto updateQuestion(Long Qid, QuestionDto questionDto) throws BadRequestException {
        Optional<Question> optionalQuestion = questionRepository.findById(Qid);

        if(!optionalQuestion.isPresent()){
            throw new BadRequestException("This question does not exists");
        }

        Optional<User> optionalUser = userRepository.findById(questionDto.getAdminId());
        Question oldQuestion = optionalQuestion.get();
        oldQuestion.setQuestion(questionDto.getQuestion());
        //TODO
        oldQuestion.setCategory(categoryRepository.findById(questionDto.getTypeOfCategory()).get());
        oldQuestion.setOpt1(questionDto.getOpt1());
        oldQuestion.setOpt2(questionDto.getOpt2());
        oldQuestion.setOpt3(questionDto.getOpt3());
        oldQuestion.setOpt4(questionDto.getOpt4());

        oldQuestion.setCorrectAns(questionDto.getCorrectAns());
        oldQuestion.setDifficulty(questionDto.getDifficulty());

        optionalUser.ifPresent(oldQuestion::setUser);
        questionRepository.save(oldQuestion);
        return questionMapper.toResponseQuestionDto(questionRepository.save(oldQuestion));
    }

    @Override
    public ResponseQuestionDto getQuestion(Long Qid) throws BadRequestException {
        if(!this.questionRepository.findById(Qid).isPresent()){
            throw new BadRequestException("This question does not exists!");
        }
        return questionMapper.toResponseQuestionDto(
                this.questionRepository.findById(Qid).get());
    }

    @Override
    public CategoryQuestionCountDto getCategoryQuestionCount() {
      CategoryQuestionCountDto categoryQuestionCountDto = new CategoryQuestionCountDto();
      int logicalQuestionCount =   questionRepository.findByCategory_CategoryNameAndIsActive("Logical",true).size();
      int technicalQuestionCount =   questionRepository.findByCategory_CategoryNameAndIsActive("technical",true).size();
      int programmingQuestionCont  =   questionRepository.findByCategory_CategoryNameAndIsActive("Programming",true).size();
      categoryQuestionCountDto.setLogicalQuestionCount(logicalQuestionCount);
      categoryQuestionCountDto.setTechnicalQuestionCount(technicalQuestionCount);
      categoryQuestionCountDto.setProgrammingQuestionCount(programmingQuestionCont);
      return categoryQuestionCountDto;
    }



}
