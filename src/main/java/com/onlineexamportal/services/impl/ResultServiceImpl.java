package com.onlineexamportal.services.impl;

import com.onlineexamportal.Mapper.ResultMapper;
import com.onlineexamportal.dto.*;
import com.onlineexamportal.entities.*;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.repositories.*;
import com.onlineexamportal.services.ExamService;
import com.onlineexamportal.services.ResultService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
@Service
@Transactional
@RequiredArgsConstructor
public class ResultServiceImpl implements ResultService {


    private final ResultMapper resultMapper;
    @Autowired
    private ResultRepository resultRepository;

    @Autowired
    private ExamService examService;
    @Autowired
    private StudentSelectedAnswerRepository studentSelectedAnswerRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private ExamRepository examRepository;
    @Autowired
    private UserRepository userRepository;


    public List<ResponseResultDto> resultList(){
        return this.resultRepository.findAll().stream()
                .map(resultMapper::toResponseResultDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseResultWithExamDetailDto getResultByStatus (UpdateResultStatusDto updateResultStatusDto) throws BadRequestException {
        ResponseResultWithExamDetailDto responseResultWithExamDetailDto = new ResponseResultWithExamDetailDto();
        responseResultWithExamDetailDto.setResponseExamDto(examService.getExam(updateResultStatusDto.getExamId()));
        responseResultWithExamDetailDto.setResponseResultDetailDtoList(
                this.resultRepository.findAllByIsResultFinalAndExam_ExamId(updateResultStatusDto.getStatus(),updateResultStatusDto.getExamId()).stream()
                .map(resultMapper::toResponseResultDetailDto)
                .collect(Collectors.toList()));
        return responseResultWithExamDetailDto;
    }

    @Override
    public ResponseResultWithExamDetailDto getResultByResultStatus(UpdateResultStatusDto updateResultStatusDto) throws BadRequestException {
        ResponseResultWithExamDetailDto responseResultWithExamDetailDto = new ResponseResultWithExamDetailDto();
        responseResultWithExamDetailDto.setResponseExamDto(examService.getExam(updateResultStatusDto.getExamId()));
        responseResultWithExamDetailDto.setResponseResultDetailDtoList(this.resultRepository.findAllByIsResultFinalAndExam_ExamIdAndStatus(true,updateResultStatusDto.getExamId(), updateResultStatusDto.getStatus()).stream()
                .map(resultMapper::toResponseResultDetailDto)
                .collect(Collectors.toList()));
        return responseResultWithExamDetailDto;
    }

    @Override
    public List<ResponseResultDetailDto> getResultsByStudentId (Long userId){
        return this.resultRepository.findAllByIsResultFinalAndUser_UserId(true, userId).stream()
                .map(resultMapper::toResponseResultDetailDto)
                .collect(Collectors.toList());

    }

    @Override
    public List<ResponseResultDto> getFinalResult (FinalResultResponseDto finalResultResponseDto){
        return this.resultRepository.findAllByIsResultFinalAndExam_ExamIdAndUser_UserId(finalResultResponseDto.getStatus(),finalResultResponseDto.getExamId(), finalResultResponseDto.getUserId()).stream()
                .map(resultMapper::toResponseResultDto)
                .collect(Collectors.toList());
    }

    @Override
    public String generateResult(RequestGenerateExamDto requestGenerateExamDto){
        requestGenerateExamDto.getSelectedAnswerToMark().forEach((selectedAnswerToMark) -> {

            StudentSelectedAnswer studentSelectedAnswer = this.studentSelectedAnswerRepository.findById(selectedAnswerToMark.getSelectedAnswerId()).get();
            studentSelectedAnswer.setMark(selectedAnswerToMark.getMark());
            this.studentSelectedAnswerRepository.save(studentSelectedAnswer);
        });
        Result result =  this.resultRepository.findByExam_ExamIdAndUser_UserId(requestGenerateExamDto.getExamId(),requestGenerateExamDto.getUserId());
        result.setProgrammingMarks(requestGenerateExamDto.getProgrammingMark());
        if(result.getExam().getProgrammingPassingMarks() <= requestGenerateExamDto.getProgrammingMark()){
            result.setProgrammingStatus(true);
            System.out.println("programming status : pass ");
        }else {

            result.setProgrammingStatus(false);
        }
        result.setStatus(result.isProgrammingStatus() && result.isLogicalStatus() && result.isTechnicalStatus());
        result.setResultFinal(true);
        resultRepository.save(result);

        return "Result generated successfully";
    }

    @Override
    public void createResult(RequestResultDto requestResultDto) throws BadRequestException {

        Result result = new Result();
        Optional<User> optionalUser = userRepository.findById(requestResultDto.getUserId());
        if (!optionalUser.isPresent()) {
            throw new BadRequestException("user not found");
        }

        List<StudentSelectedAnswer> thisStudentAnswers = studentSelectedAnswerRepository.findAllByExam_ExamIdAndUser_UserId(requestResultDto.getExamId(), requestResultDto.getUserId());
        Result checkResult = resultRepository.findByExam_ExamIdAndUser_UserId(requestResultDto.getExamId(), requestResultDto.getUserId());

        if (checkResult != null) {
            throw new BadRequestException("Result already exists");
        }


        float logicalMarks = 0;
        float technicalMarks = 0;

        List<String> studentSelectedAnswer = new ArrayList<>();


        for (int i = 0; i < thisStudentAnswers.size(); i++) {

            StudentSelectedAnswer s = thisStudentAnswers.get(i);

            Long questionId = s.getQuestion().getQuestionId();

            String correctAns = questionRepository.findById(questionId).get().getCorrectAns();

            studentSelectedAnswer.add(i+1 +" studentAns: "+ s+" CorrectAns: " +correctAns);
            if (s.getSelectedAnswer().equalsIgnoreCase(correctAns) && s.getQuestion().getCategory().getCategoryName().equalsIgnoreCase("logical")) {
                logicalMarks += s.getQuestion().getMark();
            }
            if (s.getSelectedAnswer().equalsIgnoreCase(correctAns) && s.getQuestion().getCategory().getCategoryName().equalsIgnoreCase("technical")) {
                technicalMarks += s.getQuestion().getMark();
            }

        }

        System.out.println("logical marks :" + logicalMarks);
        System.out.println("technicalMarks  :" + technicalMarks);

        boolean isLogicalPass = false;
        boolean isTechnicalPass = false;

        //Todo: Add category passing mark logic from exam table with exam Id
        Optional<Exam> optionalExam = this.examRepository.findById(requestResultDto.getExamId());
        if (!optionalExam.isPresent()) throw new BadRequestException("Exam not found");
        int logicalPassingMarks = optionalExam.get().getLogicalPassingMarks();
        int technicalPassingMarks = optionalExam.get().getTechnicalPassingMarks();
        if (logicalPassingMarks <= logicalMarks) {
            isLogicalPass = true;
        }
        if (technicalPassingMarks <= technicalMarks) {
            isTechnicalPass = true;
        }
            result.setLogicalStatus(isLogicalPass);
            result.setTechnicalStatus(isTechnicalPass);
            result.setUser(optionalUser.get());
            result.setExam(optionalExam.get());
            result.setLogicalMarks(logicalMarks);
            result.setTechnicalMarks(technicalMarks);

            result.setResultFinal(false);
        System.out.println("*****************************************************************************************");
        studentSelectedAnswer.forEach(System.out::println);
        System.out.println("*****************************************************************************************");
            resultRepository.save(result);
        }


    @Override
    public String deleteResult(Long resultId) throws BadRequestException {
        Optional<Result> optionalResult = this.resultRepository.findById(resultId);
        if(!optionalResult.isPresent()){
            throw new BadRequestException("Result does not exists !");
        }
        this.resultRepository.deleteById(resultId);
        return "Student deleted successfully";
    }

    @Override
    public RequestResultDto updateResult(Long resultId, RequestResultDto requestResultDto) throws BadRequestException {
        return null;
    }


    @Override
    public RequestResultDto getResultByResultId(Long resultId) throws BadRequestException {
      Optional<Result> optionalResult = this.resultRepository.findById(resultId);
      if(!optionalResult.isPresent()){
          throw  new BadRequestException("Result not found");
      }
        return resultMapper.toRequestResultDto(optionalResult.get());
    }
}
