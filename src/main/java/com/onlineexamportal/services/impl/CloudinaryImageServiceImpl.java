package com.onlineexamportal.services.impl;

import com.cloudinary.Cloudinary;
import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.dto.RequestQuestionImageDto;
import com.onlineexamportal.dto.ResponseQuestionDto;
import com.onlineexamportal.dto.ResponseQuestionImageDto;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.QuestionImage;
import com.onlineexamportal.entities.User;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.repositories.QuestionImageRepository;
import com.onlineexamportal.repositories.QuestionRepository;
import com.onlineexamportal.repositories.UserRepository;
import com.onlineexamportal.services.CloudinaryImageService;
import io.jsonwebtoken.lang.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class CloudinaryImageServiceImpl implements CloudinaryImageService {

    @Autowired
    private Cloudinary cloudinary;

    @Autowired
    private QuestionServiceImpl questionServiceImpl;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionImageRepository questionImageRepository;

    @Override
    public Map upload(MultipartFile multipartFile) throws IOException {
        Map data = this.cloudinary.uploader().upload(multipartFile.getBytes(), new HashMap());
        data.get("url");
        return data;
    }

    public List<Map> uploadImages(Long Qid , RequestQuestionImageDto requestQuestionImageDto) throws BadRequestException, IOException {
        Optional<Question> optionalQuestion = questionRepository.findById(Qid);

        if(!optionalQuestion.isPresent()){
            throw new BadRequestException("This question does not exists");
        }

        Question oldQuestion = optionalQuestion.get();
        QuestionImage questionImage = new QuestionImage();

        List<Map> questionImageList = new ArrayList<>();

        if(requestQuestionImageDto.getImageUrl() != null  ){
            Map imageUrl= this.cloudinary.uploader().upload(requestQuestionImageDto.getImageUrl().getBytes(),new HashMap<>());
            questionImage.setImageUrl(imageUrl.get("url").toString());
            System.out.println("imageUrl: "+imageUrl.get("url").toString());
            questionImageList.add(imageUrl);
        }

        if(requestQuestionImageDto.getOpt1ImageUrl() != null ){
            Map opt1ImageUrl= this.cloudinary.uploader().upload(requestQuestionImageDto.getOpt1ImageUrl().getBytes(),new HashMap<>());
            questionImage.setOpt1ImageUrl(opt1ImageUrl.get("url").toString());

            System.out.println("opt1ImageUrl: "+opt1ImageUrl.get("url").toString());
            questionImageList.add(opt1ImageUrl);
        }

        if( requestQuestionImageDto.getOpt2ImageUrl() != null){
            Map opt2ImageUrl= this.cloudinary.uploader().upload(requestQuestionImageDto.getOpt2ImageUrl().getBytes(),new HashMap<>());
            questionImage.setOpt2ImageUrl(opt2ImageUrl.get("url").toString());

            System.out.println("opt2ImageUrl: "+opt2ImageUrl.get("url").toString());
            questionImageList.add(opt2ImageUrl);
        }


        if(requestQuestionImageDto.getOpt3ImageUrl() != null){
            Map opt3ImageUrl= this.cloudinary.uploader().upload(requestQuestionImageDto.getOpt3ImageUrl().getBytes(),new HashMap<>());
            questionImage.setOpt3ImageUrl(opt3ImageUrl.get("url").toString());

            System.out.println("opt3ImageUrl: "+opt3ImageUrl.get("url").toString());
            questionImageList.add(opt3ImageUrl);

        }

        if( requestQuestionImageDto.getOpt4ImageUrl() != null){
            Map opt4ImageUrl= this.cloudinary.uploader().upload(requestQuestionImageDto.getOpt4ImageUrl().getBytes(),new HashMap<>());
            questionImage.setOpt4ImageUrl(opt4ImageUrl.get("url").toString());
            System.out.println("opt4ImageUrl: "+opt4ImageUrl.get("url").toString());
            questionImageList.add(opt4ImageUrl);

        }


        oldQuestion.setQuestionImage(questionImage);
        questionRepository.save(oldQuestion);
        return questionImageList;
    }
    @Override
    public Map getImage(String url) throws IOException {
        return null;
    }
}
