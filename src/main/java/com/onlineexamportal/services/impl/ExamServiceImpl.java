package com.onlineexamportal.services.impl;

import com.onlineexamportal.Mapper.ExamMapper;
import com.onlineexamportal.Mapper.ResultMapper;
import com.onlineexamportal.Mapper.UserMapper;
import com.onlineexamportal.dto.*;
import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.StudentSelectedAnswer;
import com.onlineexamportal.entities.User;
import com.onlineexamportal.enums.Difficulty;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.NotEnoughQuestionsException;
import com.onlineexamportal.repositories.*;
import com.onlineexamportal.services.ExamService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
@CrossOrigin
public class ExamServiceImpl implements ExamService {

    private final ExamMapper examMapper;

    private final ResultMapper resultMapper;

    private final UserMapper userMapper;

    @Autowired
    private ExamRepository examRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ResultRepository resultRepository;

    @Autowired
    private StudentSelectedAnswerRepository studentSelectedAnswerRepository;
    @Autowired
    private ExamStudentRepository examStudentRepository;

    @Override
    public List<ResponseExamDto> examList() {
        return this.examRepository.
                findAll().stream()
                .map(examMapper::toResponseExamDto)
                .collect(Collectors.toList());
    }

    //TODO (2)
    public List<ResponseExamDto> examStudentList() {
        return this.examStudentRepository.
                findAll().stream()
                .map(examMapper::toResponseExamDto)
                .collect(Collectors.toList());
    }



    //TODO (3)
    public List<ResponseExamDto> studentExamList(Long userId ){
        List<ResponseExamDto> responseExamDtoList = this.examRepository.findAllByUserId(userId)
                .stream().map(examMapper::toResponseExamDto).collect(Collectors.toList());

        return this.examRepository.findAllByUserId(userId)
                .stream().map(examMapper::toResponseExamDto).collect(Collectors.toList());
    }

    //TODO (4)
    public List<ResponseUserDto> remainingExamStudentList( Long examId){
        return this.examRepository.findRemainingStudents(examId).stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }




    @Override
    public List<ResponseExamDto> examListForStudent(Long studentId) {

        List<ResponseExamDto> examList = this.examRepository.
                findAll().stream()
                .map(examMapper::toResponseExamDto)
                .collect(Collectors.toList());

        List<ResponseResultDto> responseResultDtoList = this.resultRepository.findAllByUser_UserId(studentId).stream()
                .map(resultMapper::toResponseResultDto)
                .collect(Collectors.toList());

        return examList.stream().map((exam) -> {
            responseResultDtoList.forEach((result) -> {
                if(Objects.equals(result.getExamId(), exam.getExamId())) {
                    exam.setExamGiven(true);
                }
            });
            return exam;
        }).collect(Collectors.toList());
    }

    //TODO:Get questions by category (4)
//    public List<Question>QuestionListByCategoryAndDifficulty(String category , Difficulty difficulty , List<Question> availableEasyQuestion , double noOfRequestedEasyQuestion,
//                                                             List<Question>  availableModerateQuestion,double noOfRequestedModerateQuestion,  List<Question> availableAdvanceQuestion, double noOfRequestedAdvanceQuestion ,
//                                                             double noOfEasyQuestions, double noOfModerateQuestions,double noOfAdvanceQuestions,
//                                                             Exam newExam){
//        List<Question> examQuestionList = new ArrayList<>();
//        Collections.shuffle(availableEasyQuestion);
//        for (int i = 0; i < noOfEasyQuestions; i++) {
//            examQuestionList.add(availableEasyQuestion.get(i));
//        }
//        System.out.println(" Easy questions: "+(long) examQuestionList.size());
//
//        Collections.shuffle(availableModerateQuestion);
//        for (int i = 0; i < noOfModerateQuestions; i++) {
//            examQuestionList.add(availableModerateQuestion.get(i));
//        }
//        System.out.println(" Moderate questions: "+(long) examQuestionList.size());
//
//        Collections.shuffle(availableAdvanceQuestion);
//        for (int i = 0; i < noOfAdvanceQuestions; i++) {
//            examQuestionList.add(availableAdvanceQuestion.get(i));
//        }
//        System.out.println(" Advance questions: "+(long) examQuestionList.size());
//        newExam.setExamQuestionList(examQuestionList);
//        examRepository.save(newExam);
//        return examQuestionList;
//    }
//
    //TODO:Get questions by category (3)
//    public List<Question> getQuestionsByCategoryAndDifficulty(  String category , Difficulty difficulty , List<Question> availableEasyQuestion , double noOfRequestedEasyQuestion, List<Question>  availableModerateQuestion,double noOfRequestedModerateQuestion,  List<Question> availableAdvanceQuestion, double noOfRequestedAdvanceQuestion , Exam newExam){
//
//        List<Question> examQuestionList = new ArrayList<>();
//        if(Objects.equals(category, "Logical") && difficulty == Difficulty.EASY){
//
//            double noOfEasyLogicalQuestions =  Math.ceil(noOfRequestedEasyQuestion /2);
//            double noOfModerateLogicalQuestions =  Math.ceil(noOfRequestedEasyQuestion /4);
//            double noOfAdvanceLogicalQuestions =  Math.floor(noOfRequestedEasyQuestion/4);
//
//            return QuestionListByCategoryAndDifficulty(category,difficulty,  availableEasyQuestion ,  noOfRequestedEasyQuestion,
//             availableModerateQuestion, noOfRequestedModerateQuestion,   availableAdvanceQuestion,  noOfRequestedAdvanceQuestion ,noOfEasyLogicalQuestions,noOfModerateLogicalQuestions,noOfAdvanceLogicalQuestions,newExam);
//
//
//        }else if(Objects.equals(category, "Logical") && difficulty == Difficulty.MODERATE){
//
//            double noOfEasyLogicalQuestions =  Math.ceil(noOfRequestedModerateQuestion /4);
//            double noOfModerateLogicalQuestions =  Math.ceil(noOfRequestedModerateQuestion /2);
//            double noOfAdvanceLogicalQuestions =  Math.floor(noOfRequestedModerateQuestion/4);
//
//            return QuestionListByCategoryAndDifficulty(category,difficulty,  availableEasyQuestion ,  noOfRequestedEasyQuestion,
//                    availableModerateQuestion, noOfRequestedModerateQuestion,  availableAdvanceQuestion,  noOfRequestedAdvanceQuestion ,noOfEasyLogicalQuestions,noOfModerateLogicalQuestions,noOfAdvanceLogicalQuestions,newExam);
//
//        }else{
//
//            double noOfEasyLogicalQuestions =  Math.ceil(noOfRequestedAdvanceQuestion /4);
//            double noOfModerateLogicalQuestions =  Math.floor(noOfRequestedAdvanceQuestion /4);
//            double noOfAdvanceLogicalQuestions =  Math.ceil(noOfRequestedAdvanceQuestion /2);
//
//            return QuestionListByCategoryAndDifficulty(category,difficulty,  availableEasyQuestion ,  noOfRequestedEasyQuestion,
//                    availableModerateQuestion, noOfRequestedModerateQuestion,   availableAdvanceQuestion,  noOfRequestedAdvanceQuestion ,noOfEasyLogicalQuestions,noOfModerateLogicalQuestions,noOfAdvanceLogicalQuestions,newExam);
//
//        }
//    }

    @Override
    public void createExam(RequestExamDto requestExamDto) throws BadRequestException, NotEnoughQuestionsException {

        Exam newExam = new Exam();
        Optional<User> optionalUser = userRepository.findById(requestExamDto.getAdminId());
        if (!optionalUser.isPresent()) {
            throw new BadRequestException("Admin not present");
        }
        newExam.setUser(optionalUser.get());

        newExam.setStartDate(requestExamDto.getStartDate());
        newExam.setEndDate(requestExamDto.getEndDate());

        newExam.setDate(requestExamDto.getDate());
        newExam.setNoOfLogicalQuestion(requestExamDto.getNoOfLogicalQuestion());
        newExam.setNoOfTechnicalQuestion(requestExamDto.getNoOfLogicalQuestion());
        newExam.setNoOfProgrammingQuestion(requestExamDto.getNoOfProgrammingQuestion());
        newExam.setExamName(requestExamDto.getExamName());
        newExam.setTotalMarks(requestExamDto.getTotalMarks());
        newExam.setLogicalPassingMarks(requestExamDto.getLogicalPassingMarks());
        newExam.setTechnicalPassingMarks(requestExamDto.getTechnicalPassingMarks());
        newExam.setProgrammingPassingMarks(requestExamDto.getProgrammingPassingMarks());

        newExam.setProgrammingQuestionDifficulty(requestExamDto.getProgrammingDifficulty());
        newExam.setTechnicalQuestionDifficulty(requestExamDto.getTechnicalDifficulty());
        newExam.setLogicalQuestionDifficulty(requestExamDto.getLogicalDifficulty());

        newExam.setDuration(requestExamDto.getDuration());
        newExam.setStartTime(requestExamDto.getStartTime());
        newExam.setEndTime(requestExamDto.getEndTime());
        newExam.setTotalMarks(requestExamDto.getTotalMarks());

        int noOfLogicalQuestionInQuestionBank = questionRepository.findByCategory_CategoryId(4L).size();
        int noOfTechnicalQuestionInQuestionBank = questionRepository.findByCategory_CategoryId(3L).size();
        int noOfProgrammingQuestionInQuestionBank = questionRepository.findByCategory_CategoryId(5L).size();

        int requestedProgrammingQuestion = requestExamDto.getNoOfProgrammingQuestion();
        int requestedLogicalQuestion = requestExamDto.getNoOfLogicalQuestion();
        int requestedTechnicalQuestion = requestExamDto.getNoOfTechnicalQuestion();


        if (noOfProgrammingQuestionInQuestionBank < requestedProgrammingQuestion) {
            throw new NotEnoughQuestionsException("Not enough programming question ,please add " + (requestedProgrammingQuestion - noOfProgrammingQuestionInQuestionBank) + " questions in question bank");
        }
        if (noOfTechnicalQuestionInQuestionBank < requestedTechnicalQuestion) {
            throw new NotEnoughQuestionsException("Not enough Technical question ,please add " + (requestedTechnicalQuestion - noOfTechnicalQuestionInQuestionBank) + " questions in question bank");
        }
        if (noOfLogicalQuestionInQuestionBank < requestedLogicalQuestion) {
            throw new NotEnoughQuestionsException("Not enough Logical question ,please add " + (requestedLogicalQuestion - noOfLogicalQuestionInQuestionBank) + " questions in question bank");
        }

        List<Question> examQuestionList = new ArrayList<>();

        List<Question>logicalQuestion = questionRepository.findByCategory_CategoryName("Logical");
        List<Question>technicalQuestion = questionRepository.findByCategory_CategoryName("technical");
        List<Question>programmingQuestion = questionRepository.findByCategory_CategoryName("Programming");

        //TODO:Get questions by category (1)

//        List<Question>availableLogicalEasyQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"Logical","EASY");
//        List<Question>availableLogicalModerateQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"Logical","MODERATE");
//        List<Question>availableLAdvanceQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"Logical","ADVANCE");
//
//
//        List<Question>availableTechnicalEasyQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"technical","EASY");
//        List<Question>availableTechnicalModerateQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"technical","MODERATE");
//        List<Question>availableTechnicalAdvanceQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"technical","ADVANCE");
//
//
//        List<Question>availableProgrammingEasyQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"Programming","EASY");
//        List<Question>availableProgrammingModerateQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"Programming","MODERATE");
//        List<Question>availableProgrammingAdvanceQuestions = questionRepository.findAllByIsActiveAndCategory_CategoryNameAndDifficulty(true,"Programming","ADVANCE");



        //TODO:Get questions by category (2)

//        if(requestExamDto.getLogicalDifficulty() == Difficulty.EASY){
//
//            double noOfEasyLogicalQuestions =  Math.ceil((double) requestExamDto.getNoOfLogicalQuestion() /2);
//            double noOfModerateLogicalQuestions =  Math.ceil((double) requestExamDto.getNoOfLogicalQuestion() /4);
//            double noOfAdvanceLogicalQuestions =  Math.floor((double) requestExamDto.getNoOfLogicalQuestion() /4);
//
//
//            Collections.shuffle(logicalEasyQuestions);
//            for (int i = 0; i < noOfEasyLogicalQuestions; i++) {
//                examQuestionList.add(logicalEasyQuestions.get(i));
//            }
//            System.out.println("Logical Easy questions: "+(long) examQuestionList.size());
//
//            Collections.shuffle(logicalModerateQuestions);
//            for (int i = 0; i < noOfModerateLogicalQuestions; i++) {
//                examQuestionList.add(logicalModerateQuestions.get(i));
//            }
//            System.out.println("Logical Moderate questions: "+(long) examQuestionList.size());
//
//            Collections.shuffle(logicalAdvanceQuestions);
//            for (int i = 0; i < noOfAdvanceLogicalQuestions; i++) {
//                examQuestionList.add(logicalAdvanceQuestions.get(i));
//            }
//            System.out.println("Logical Advance questions: "+(long) examQuestionList.size());
//            newExam.setExamQuestionList(examQuestionList);
//            examRepository.save(newExam);
//
//        }else if(requestExamDto.getLogicalDifficulty() == Difficulty.MODERATE){
//
//            double noOfEasyLogicalQuestions =  Math.ceil((double) requestExamDto.getNoOfLogicalQuestion() /4);
//            double noOfModerateLogicalQuestions =  Math.ceil((double) requestExamDto.getNoOfLogicalQuestion() /2);
//            double noOfAdvanceLogicalQuestions =  Math.floor((double) requestExamDto.getNoOfLogicalQuestion() /4);
//
//            Collections.shuffle(logicalEasyQuestions);
//            for (int i = 0; i < noOfEasyLogicalQuestions; i++) {
//                examQuestionList.add(logicalEasyQuestions.get(i));
//            }
//            System.out.println("Logical Easy questions: "+(long) examQuestionList.size());
//
//            Collections.shuffle(logicalModerateQuestions);
//            for (int i = 0; i < noOfModerateLogicalQuestions; i++) {
//                examQuestionList.add(logicalModerateQuestions.get(i));
//            }
//            System.out.println("Logical Moderate questions: "+(long) examQuestionList.size());
//
//            Collections.shuffle(logicalAdvanceQuestions);
//            for (int i = 0; i < noOfAdvanceLogicalQuestions; i++) {
//                examQuestionList.add(logicalAdvanceQuestions.get(i));
//            }
//            System.out.println("Logical Advance questions: "+(long) examQuestionList.size());
//            newExam.setExamQuestionList(examQuestionList);
//            examRepository.save(newExam);
//
//        }else{
//
//            double noOfEasyLogicalQuestions =  Math.ceil((double) requestExamDto.getNoOfLogicalQuestion() /4);
//            double noOfModerateLogicalQuestions =  Math.floor((double) requestExamDto.getNoOfLogicalQuestion() /4);
//            double noOfAdvanceLogicalQuestions =  Math.ceil((double) requestExamDto.getNoOfLogicalQuestion() /2);
//
//            Collections.shuffle(logicalEasyQuestions);
//            for (int i = 0; i < noOfEasyLogicalQuestions; i++) {
//                examQuestionList.add(logicalEasyQuestions.get(i));
//            }
//            System.out.println("Logical Easy questions: "+(long) examQuestionList.size());
//
//            Collections.shuffle(logicalModerateQuestions);
//            for (int i = 0; i < noOfModerateLogicalQuestions; i++) {
//                examQuestionList.add(logicalModerateQuestions.get(i));
//            }
//            System.out.println("Logical Moderate questions: "+(long) examQuestionList.size());
//
//            Collections.shuffle(logicalAdvanceQuestions);
//            for (int i = 0; i < noOfAdvanceLogicalQuestions; i++) {
//                examQuestionList.add(logicalAdvanceQuestions.get(i));
//            }
//            System.out.println("Logical Advance questions: "+(long) examQuestionList.size());
//            newExam.setExamQuestionList(examQuestionList);
//            examRepository.save(newExam);
//        }

        /* --------------------------------------------------------------------------*/
        Collections.shuffle(logicalQuestion);
        for (int i = 0; i < requestExamDto.getNoOfLogicalQuestion(); i++) {
            examQuestionList.add(logicalQuestion.get(i));
        }
        System.out.println((long) examQuestionList.size());

        Collections.shuffle(technicalQuestion);
        for (int i = 0; i < requestExamDto.getNoOfTechnicalQuestion(); i++) {
            examQuestionList.add(technicalQuestion.get(i));
        }
        System.out.println((long) examQuestionList.size());

        Collections.shuffle(programmingQuestion);
        for (int i = 0; i < requestExamDto.getNoOfProgrammingQuestion(); i++) {
            examQuestionList.add(programmingQuestion.get(i));
        }
        System.out.println((long) examQuestionList.size());
        newExam.setExamQuestionList(examQuestionList);
        examRepository.save(newExam);
        /* --------------------------------------------------------------------------*/

    }



    //TODO v

    public ResponseExamDto addExamStudent(Long examId, List<Long> studentIds) throws BadRequestException {
        Optional<Exam> optionalExam = this.examRepository.findById(examId);
        if (!optionalExam.isPresent()) {
            throw new BadRequestException("Exam does not exists");
        }
        Exam exam = optionalExam.get();
        List<User> users = studentIds.stream().map(id->userRepository.findById(id).orElse(null)).collect(Collectors.toList());
        System.out.println(users.size());
        exam.getExamStudentList().addAll(users);
        System.out.println(exam.getExamStudentList().size());
        examRepository.save(exam);
        return examMapper.toResponseExamDto(exam);
    }

    //TODO ^







    @Override
    public String deleteExam(Long examId) throws BadRequestException {
        Optional<Exam> optionalExam = this.examRepository.findById(examId);
        if (!optionalExam.isPresent()) {
            throw new BadRequestException("Exam does not exists");
        }
        this.examRepository.deleteById(examId);
        return "Exam deleted successfully";
    }

    @Override
    public ResponseExamDto updateExam(Long examId, ResponseExamDto responseExamDto) throws BadRequestException {
        Optional<Exam> optionalExam = this.examRepository.findById(examId);
        if (!optionalExam.isPresent()) {
            throw new BadRequestException("Exam does not exists");
        }
        Exam exam = optionalExam.get();
        exam.setDuration(responseExamDto.getDuration());
        exam.setExamId(examId);
        Optional<User> optionalUser = userRepository.getByUserName(responseExamDto.getUserName());
        if (!optionalUser.isPresent()) {
            throw new BadRequestException("Admin not present");
        }

        exam.setUser(optionalUser.get());
        exam.setNoOfLogicalQuestion(responseExamDto.getNoOfLogicalQuestion());
        exam.setNoOfTechnicalQuestion(responseExamDto.getNoOfLogicalQuestion());
        exam.setNoOfProgrammingQuestion(responseExamDto.getNoOfProgrammingQuestion());

        exam.setTotalMarks(responseExamDto.getTotalMarks());
        examRepository.save(exam);
        return examMapper.toResponseExamDto(exam);
    }

    @Override
    public ResponseExamDto getExam(Long examId) throws BadRequestException {
        Optional<Exam> optionalExam = this.examRepository.findById(examId);
        if (!optionalExam.isPresent()) {
            throw new BadRequestException("Exam does not exists");
        }
        return examMapper.toResponseExamDto(optionalExam.get());
    }

    @Override
    public List<ResponseExamDto> showMyExam(Long adminId) throws BadRequestException {
        return this.examRepository.
                findAll().stream().filter(exam -> {
                    return exam.getUser().getUserId().equals(adminId);
                })
                .map(examMapper::toResponseExamDto)
                .collect(Collectors.toList());

    }

    @Override
    public List<QuestionToSelectedAnswerDto> getExamDetailForStudent(RequestResultDto requestResultDto) throws BadRequestException {
        Optional<Exam> optionalExam = this.examRepository.findById(requestResultDto.getExamId());
        if (!optionalExam.isPresent()) {
            throw new BadRequestException("Exam does not exists");
        }
        ResponseExamDto responseExamDto = examMapper.toResponseExamDto(optionalExam.get());
        List<QuestionToSelectedAnswerDto> questionToSelectedAnswerDtoList = new ArrayList<>();

        responseExamDto.getQuestionDtoList().forEach((questionDto) -> {
            StudentSelectedAnswer studentSelectedAnswer = studentSelectedAnswerRepository.findByExam_ExamIdAndUser_UserIdAndQuestion_QuestionId(requestResultDto.getExamId(), requestResultDto.getUserId(), questionDto.getQuestionId());

            questionToSelectedAnswerDtoList.add(
                    new QuestionToSelectedAnswerDto(
                          questionDto,
                          studentSelectedAnswer.getMark(),
                          studentSelectedAnswer.getSelectedAnswer()
                    )
            );
        });
        return questionToSelectedAnswerDtoList;
    }


    public void handleFileUpload(String fName,  String lName, String imgName,  String scanImageFile ) {

        String base64= scanImageFile;

        byte[] data = DatatypeConverter.parseBase64Binary(base64);
        String path;
        path = "C:/ROIMA/ONLINE EXAM PORTAL/spring boot/New folder (2)/OnlineExamPortal/src/main/java/com/onlineexamportal/uploads/questionImages"+"file.jpg";
        File file = new File(path);

        try(OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))){
            outputStream.write(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}


