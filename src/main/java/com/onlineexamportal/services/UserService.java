package com.onlineexamportal.services;

import com.onlineexamportal.dto.*;
import com.onlineexamportal.exceptions.BadRequestException;
import com.onlineexamportal.exceptions.UnauthorizedException;

import java.util.List;

public interface UserService {


    public void createUser(ResponseUserDto responseUserDto) throws BadRequestException;
    public String deleteUser(Long userId) throws BadRequestException;
    public ResponseUserDto updateUser(Long userId, ResponseUserDto responseUserDto) throws BadRequestException;
    public ResponseUserDto getUserByUserName(String userName) throws BadRequestException;
    public List<ResponseUserStudentDto> getAllUserStudent();
    public ResponseUserDto Login(LoginRequestDto loginRequestDto) throws BadRequestException, UnauthorizedException;
    public void disableStudent(Long userId);
    public ResponseUserDto getUserByUserId(Long userId) throws BadRequestException;

    AdminDashboardDto getAdminDashboard();
    StudentDashboardDto getStudentDashboard(Long studentId);
}
