package com.onlineexamportal.services;

import com.onlineexamportal.dto.CategoryQuestionCountDto;
import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.dto.ResponseQuestionDto;
import com.onlineexamportal.exceptions.BadRequestException;

import java.util.List;

public interface QuestionService {
    public ResponseQuestionDto createQuestion(QuestionDto questionDto) throws BadRequestException;
    public String deleteQuestion(Long questionId) throws BadRequestException;
    public ResponseQuestionDto updateQuestion(Long questionId, QuestionDto questionDto) throws BadRequestException;
    public ResponseQuestionDto getQuestion(Long questionId) throws BadRequestException;
    public CategoryQuestionCountDto getCategoryQuestionCount();
    public List<ResponseQuestionDto> getProgrammingQuestionList();
    public List<ResponseQuestionDto> getMcqQuestionList();
    public List<ResponseQuestionDto> questionList();
}

