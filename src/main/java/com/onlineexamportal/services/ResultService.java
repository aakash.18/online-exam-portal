package com.onlineexamportal.services;

import com.onlineexamportal.dto.*;
import com.onlineexamportal.exceptions.BadRequestException;

import java.util.List;

public interface ResultService {


    public void createResult(RequestResultDto requestResultDto) throws BadRequestException;
    public String deleteResult(Long resultId) throws BadRequestException;
    public RequestResultDto updateResult(Long resultId, RequestResultDto requestResultDto) throws BadRequestException;
    public RequestResultDto getResultByResultId(Long resultId) throws BadRequestException;
    public String generateResult(RequestGenerateExamDto requestGenerateExamDto);
    public List<ResponseResultDto> getFinalResult (FinalResultResponseDto finalResultResponseDto);
    public ResponseResultWithExamDetailDto getResultByStatus (UpdateResultStatusDto updateResultStatusDto) throws BadRequestException;

    ResponseResultWithExamDetailDto getResultByResultStatus(UpdateResultStatusDto updateResultStatusDto) throws BadRequestException;

    public List<ResponseResultDetailDto> getResultsByStudentId (Long studentId);


}
