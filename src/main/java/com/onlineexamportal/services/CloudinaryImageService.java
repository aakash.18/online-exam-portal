package com.onlineexamportal.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CloudinaryImageService {
    public Map upload(MultipartFile multipartFile) throws IOException;
    public Map getImage(String url) throws IOException;

}
