package com.onlineexamportal.enums;

public enum UserRole {
    ADMIN, STUDENT
}
