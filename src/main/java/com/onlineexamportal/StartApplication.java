package com.onlineexamportal;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication
@PropertySource({"application.properties"})
public class StartApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class,args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Running....");
    }
}
