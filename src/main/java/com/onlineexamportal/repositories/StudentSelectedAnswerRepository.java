package com.onlineexamportal.repositories;

import com.onlineexamportal.entities.StudentSelectedAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentSelectedAnswerRepository extends JpaRepository<StudentSelectedAnswer,Long> {

    public List<StudentSelectedAnswer> findAllByExam_ExamIdAndUser_UserId(Long examId,Long userId);
    public List<StudentSelectedAnswer> findAllByExam_ExamIdAndUser_UserIdAndQuestion_Category_CategoryId(Long examId,Long userId, Long categoryId);
    public StudentSelectedAnswer findByExam_ExamIdAndUser_UserIdAndQuestion_QuestionId(Long examId, Long userId, Long questionId);

}
