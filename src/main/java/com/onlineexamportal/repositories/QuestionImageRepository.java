package com.onlineexamportal.repositories;

import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.QuestionImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionImageRepository extends JpaRepository<QuestionImage,Long> {
}
