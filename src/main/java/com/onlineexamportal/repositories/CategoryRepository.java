package com.onlineexamportal.repositories;

import com.onlineexamportal.entities.Category;
import com.onlineexamportal.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category , Long> {

    public Category getByCategoryName(String username);
 
}
