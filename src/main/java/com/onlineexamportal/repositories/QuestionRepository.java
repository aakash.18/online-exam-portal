package com.onlineexamportal.repositories;

import com.onlineexamportal.entities.Category;
import com.onlineexamportal.entities.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question,Long> {
    public List<Question> findByCategory_CategoryName(String category);
    public List<Question> findByCategory_CategoryNameOrCategory_CategoryName(String logical,String technical);
    public List<Question>findByCategory_CategoryId(Long id);
    public List<Question> findAllByIsActive(boolean isActive);
    public Page<Question> findAllByIsActive(boolean isActive , Pageable  pageable);
    public List<Question> findAllByIsActiveAndCategory_CategoryId(boolean isActive, Long id);
    public List<Question> findAllByIsActiveAndCategory_CategoryNameAndDifficulty(boolean isActive, String category,String difficulty);
    public List<Question> findByCategory_CategoryNameAndIsActive(String name,boolean isActive);

    public List<Question>  findByQuestionContainingIgnoreCase( String question);

}
