package com.onlineexamportal.repositories;

import com.onlineexamportal.dto.ResponseExamDto;
import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamRepository extends JpaRepository<Exam,Long> {
    public User getByUser_UserId(Long userId);

    @Query(value =
            " SELECT * FROM exam JOIN exam_student ON" +
                    " exam_student.exam_id =exam.exam_id\n" +
                    " LEFT JOIN user on exam_student.user_id = user.user_id " +
                    " WHERE user.user_id = :userId",
            nativeQuery = true)
    public List<Exam> findAllByUserId(@Param("userId") Long userId);


    @Query(value =
            " SELECT * FROM user WHERE  user.user_role = \"STUDENT\" "
                    + "AND user.user_id NOT IN ( SELECT user.user_id FROM " +
                    "  user  JOIN exam_student ON " +
                    "  exam_student.user_id = user.user_id \n" +
                    "  WHERE exam_student.exam_id = :examId)",
            nativeQuery = true)
    public  List<User> findRemainingStudents(@Param("examId") Long examId);

}
