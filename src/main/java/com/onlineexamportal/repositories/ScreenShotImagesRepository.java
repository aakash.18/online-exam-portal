package com.onlineexamportal.repositories;

import com.onlineexamportal.entities.ScreenShotImages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ScreenShotImagesRepository extends JpaRepository<ScreenShotImages, Long> {
    Optional<ScreenShotImages> findByName(String fileName);

    @Query(value = "SELECT * FROM screen_shot_images WHERE  student_id =:studentId AND exam_id =:examId",nativeQuery = true)
   List<ScreenShotImages> findAllByStudentIdAndExamId(Long studentId , Long examId);
}
