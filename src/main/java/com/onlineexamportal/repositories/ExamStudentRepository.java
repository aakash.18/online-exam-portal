package com.onlineexamportal.repositories;

import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamStudentRepository extends JpaRepository<Exam,Long> {


//    public Exam findByExamIdAndUserId(Long examId ,Long userId);
}
