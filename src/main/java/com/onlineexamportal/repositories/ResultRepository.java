package com.onlineexamportal.repositories;

import com.onlineexamportal.entities.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result,Long> {
    public  Result  findByExam_ExamIdAndUser_UserId(Long examId,Long userId);
    public List<Result> findAllByIsResultFinalAndExam_ExamId(boolean isResultFinal,Long examId);
    public List<Result> findAllByIsResultFinalAndExam_ExamIdAndUser_UserId(boolean isResultFinal ,Long examId, Long userId);

    List<Result> findAllByIsResultFinalAndUser_UserId(boolean isResultFinal, Long userId);

    List<Result> findAllByUser_UserId(Long userId);

    List<Result> findAllByUser_UserIdAndIsResultFinalAndStatus(Long userId, boolean isResultFinal, boolean status);

    List<Result> findAllByIsResultFinalAndExam_ExamIdAndStatus(boolean isResultFinal, Long examId, boolean status);

}
