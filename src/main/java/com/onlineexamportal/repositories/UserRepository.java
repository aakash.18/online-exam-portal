package com.onlineexamportal.repositories;

import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.User;
import com.onlineexamportal.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    public Optional<User> getByUserName(String username);
    public void deleteByUserName(String username);
    public List<User> findAllByUserRole(UserRole userRole);
    public List<User> findAllByIsActive(boolean isActive);
    public User findByUserIdAndIsActive(Long userId ,boolean isActive);

    public List<User>  findAllByUserRoleAndIsActive(UserRole userRole , boolean isActive);
    public List<Exam> findAllByUserId(Long userId);

    @Query(value =
            " SELECT * FROM user WHERE  user.user_role = \"STUDENT\" "
                    + "AND user.user_id NOT IN ( SELECT user.user_id FROM " +
                    "  user  JOIN exam_student ON " +
                    "  exam_student.user_id = user.user_id \n" +
                    "  WHERE exam_student.exam_id = :examId)",
            nativeQuery = true)
    public  List<User> findRemainingStudents(@Param("examId") Long examId);

    @Query(value =
          "SELECT * FROM  user  JOIN exam_student " +
                  " ON exam_student.user_id = user.user_id \n" +
                  " WHERE exam_student.exam_id = :examId" +
                  " AND  user.user_role = \"STUDENT\"",
            nativeQuery = true)
    public  List<User> findEnrolledStudents(@Param("examId") Long examId);

    @Query(value =
            "DELETE FROM exam_student WHERE exam_id=:examId AND user_id=:userId",
            nativeQuery = true)
    public void removeStudentFromExam(@Param("examId")Long examId,@Param("userId") Long userId);




}
