package com.onlineexamportal.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminDashboardDto {
    private int totalStudents;
    private int totalQuestions;
    private int totalExams;
    private int totalLogicalQuestions;
    private int totalProgrammingQuestions;
    private int totalTechnicalQuestions;
}
