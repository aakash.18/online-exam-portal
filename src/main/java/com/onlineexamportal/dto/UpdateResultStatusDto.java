package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateResultStatusDto {
    Boolean status;
    Long examId;
}
