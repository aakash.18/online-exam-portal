package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FinalResultResponseDto {
    private Long examId;
    private Long userId;
    private Boolean status;
}
