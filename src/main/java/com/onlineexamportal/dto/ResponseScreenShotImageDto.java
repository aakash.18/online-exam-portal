package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseScreenShotImageDto {
    byte[] screenShotImage;
    Long examId;
    Long studentId;
}
