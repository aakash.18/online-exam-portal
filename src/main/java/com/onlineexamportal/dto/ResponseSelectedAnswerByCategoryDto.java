package com.onlineexamportal.dto;

import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Getter
@Setter
public class ResponseSelectedAnswerByCategoryDto {
    private Long studentSelectedAnswerId;
    private String selectedAnswer;
    private Long examId;
    private ResponseUserDto userDto;
    private ResponseQuestionDto questionDto;
}
