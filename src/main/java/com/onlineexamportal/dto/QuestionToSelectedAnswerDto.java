package com.onlineexamportal.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class QuestionToSelectedAnswerDto {
    private ResponseQuestionDto responseQuestionDto;
    private int obtainedMark;
    private String studentSelectedAnswer;
}
