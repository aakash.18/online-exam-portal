package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseCategoryDto {

    private Long categoryId;
    private String categoryName;

}
