package com.onlineexamportal.dto;

import com.onlineexamportal.entities.Category;
import com.onlineexamportal.entities.Exam;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ManyToOne;
@Getter
@Setter
public class ResponseCategoryPassingMarksDto {

    private Long minMarksId;
    private Long examId;
    private float minMarks;
    private Long categoryId;

    private Exam exam;

    private Category category;
}
