package com.onlineexamportal.dto;

import com.onlineexamportal.entities.User;
import com.onlineexamportal.enums.Category;
import com.onlineexamportal.enums.Difficulty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ResponseExamDto {


    private Long examId;
    private String userName;
    private String examName;
    private Long duration;

    private int noOfLogicalQuestion;
    private int noOfTechnicalQuestion;
    private int noOfProgrammingQuestion;

    private String date;
    private Long startDate;
    private Long endDate;

    private int totalMarks;
    private int technicalPassingMarks;
    private int logicalPassingMarks;
    private int programmingPassingMarks;

    private Difficulty logicalDifficulty;
    private Difficulty technicalDifficulty;
    private Difficulty programmingDifficulty;

    private Long startTime;
    private Long endTime;

    private List<ResponseQuestionDto> questionDtoList;
    private List<RequestResultDto> requestResultDtoList;
    private List<ResponseUserDto> responseUserDtoList;
    boolean isExamGiven;
}
