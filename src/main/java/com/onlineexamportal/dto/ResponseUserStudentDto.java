package com.onlineexamportal.dto;

import com.onlineexamportal.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResponseUserStudentDto {
    private Long userId;
    private String userName;
    private String name;
    private UserRole userRole;
    private boolean isActive;
    private String password;
}
