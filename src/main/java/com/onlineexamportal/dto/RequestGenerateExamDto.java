package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RequestGenerateExamDto {
    private Long examId;
    private Long userId;
    private int programmingMark;
    private List<RequestSelectedAnswerToMarkDto> selectedAnswerToMark;
}
