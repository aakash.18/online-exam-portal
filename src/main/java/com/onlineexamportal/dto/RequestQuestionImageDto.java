package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class RequestQuestionImageDto {
    private MultipartFile imageUrl;
    private MultipartFile opt1ImageUrl;
    private MultipartFile opt2ImageUrl;
    private MultipartFile opt3ImageUrl;
    private MultipartFile opt4ImageUrl;
}
