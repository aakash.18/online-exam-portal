package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseResultDto {
    private String userName; // mapping -> from userObject
    private Long resultId;
    private Long examId;
    private Long userId;
    private float logicalMarks;
    private float technicalMarks;
    private float programmingMarks;
    private boolean logicalStatus;
    private boolean technicalStatus;
    private boolean programmingStatus;
    private boolean status;
}

