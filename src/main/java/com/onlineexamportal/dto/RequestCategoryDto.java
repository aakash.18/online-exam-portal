package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestCategoryDto {
    private String categoryName;
}
