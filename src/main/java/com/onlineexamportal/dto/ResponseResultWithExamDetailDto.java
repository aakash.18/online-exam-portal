package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResponseResultWithExamDetailDto {
    private ResponseExamDto responseExamDto;
    private List<ResponseResultDetailDto> responseResultDetailDtoList;
}
