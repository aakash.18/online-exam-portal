package com.onlineexamportal.dto;

import com.onlineexamportal.enums.Category;
import com.onlineexamportal.enums.Difficulty;
import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
public class RequestExamDto {

    private Long adminId;
    private Long duration;
    private int noOfLogicalQuestion;
    private int noOfTechnicalQuestion;
    private int noOfProgrammingQuestion;
    private String examName;
    private Date date;

    private Long startDate;
    private Long endDate;


    private int totalMarks;
    private int technicalPassingMarks;
    private int logicalPassingMarks;
    private int programmingPassingMarks;
    private Difficulty logicalDifficulty;
    private Difficulty technicalDifficulty;
    private Difficulty programmingDifficulty;
    private Long startTime;
    private Long endTime;
    private String typeOfCategory;
}
