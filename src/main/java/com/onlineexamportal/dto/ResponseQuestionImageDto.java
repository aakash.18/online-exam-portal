package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class ResponseQuestionImageDto {
    private String imageUrl;
    private String opt1ImageUrl;
    private String opt2ImageUrl;
    private String opt3ImageUrl;
    private String opt4ImageUrl;
}
