package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestStudentSelectedAnswerDto {

    private String selectedAnswer;
    private Long examId;
    private Long userId;
    private Long questionId;
}
