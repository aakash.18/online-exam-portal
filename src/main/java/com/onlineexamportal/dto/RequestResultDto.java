package com.onlineexamportal.dto;

import lombok.*;

@Getter
@Setter
public class RequestResultDto {


    private Long examId;
    private Long userId;

}
