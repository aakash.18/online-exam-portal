package com.onlineexamportal.dto.ApiResponse;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class SuccessResponseDTO {
    private int status;
    private Object data;
}
