package com.onlineexamportal.dto;

import com.onlineexamportal.entities.QuestionImage;
import com.onlineexamportal.enums.Category;
import com.onlineexamportal.enums.Difficulty;

import lombok.*;

import java.util.List;

@Getter
@Setter
public class QuestionDto {

    private String question;
    private String opt1;
    private String opt2;
    private String opt3;
    private String opt4;
    private String correctAns;
    private Difficulty difficulty;
    private String imgUrls;
    private Long typeOfCategory;
    private  int mark;
    private Long adminId;

    private String imageUrl;
    private String opt1ImageUrl;
    private String opt2ImageUrl;
    private String opt3ImageUrl;
    private String opt4ImageUrl;

}
