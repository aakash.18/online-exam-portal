package com.onlineexamportal.dto;

import com.onlineexamportal.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RequestUserDto {

    private String userName;
    private String password;
    private String name;
    private UserRole userRole;

}
