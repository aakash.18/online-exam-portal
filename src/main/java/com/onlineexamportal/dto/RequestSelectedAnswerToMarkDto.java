package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestSelectedAnswerToMarkDto {
    private Long selectedAnswerId;
    private int mark;
}
