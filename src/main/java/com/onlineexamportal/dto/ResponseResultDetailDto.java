package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseResultDetailDto {
    private ResponseResultDto responseResultDto;
    private ResponseExamDto responseExamDto;
}
