package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class StudentDashboardDto {
    private int totalGivenExams;
    private int totalPassedExams;
    private int totalFailedExams;
    private List<ResponseResultDetailDto> responseResultDetailDtoList;
}
