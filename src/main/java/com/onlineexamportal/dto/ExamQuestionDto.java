package com.onlineexamportal.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExamQuestionDto {
    private Long userId;
    private Long examId;
    private int noOfLogicalQuestion;
    private int noOfTechnicalQuestion;
    private int noOfProgrammingQuestion;
}
