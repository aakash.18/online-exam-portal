package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestSelectedAnswerByCategoryDto {
    private Long examId;
    private Long userId;
    private Long categoryId;
}

