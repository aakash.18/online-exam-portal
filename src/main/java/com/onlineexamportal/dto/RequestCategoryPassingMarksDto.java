package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestCategoryPassingMarksDto {
    private float minMarks;
    private Long categoryId;
    private Long examId;
}
