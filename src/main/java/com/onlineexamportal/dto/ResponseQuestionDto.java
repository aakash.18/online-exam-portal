package com.onlineexamportal.dto;

import com.onlineexamportal.enums.Difficulty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseQuestionDto {
    private String question;
    private String opt1;
    private String opt2;
    private String opt3;
    private String opt4;

    private String imageUrl;
    private String opt1ImageUrl;
    private String opt2ImageUrl;
    private String opt3ImageUrl;
    private String opt4ImageUrl;


    private String correctAns;
    private Difficulty difficulty;
    private String imgUrls;
    private String typeOfCategory;
    private  int mark;
    private String admin;
    private Long questionId;

}
