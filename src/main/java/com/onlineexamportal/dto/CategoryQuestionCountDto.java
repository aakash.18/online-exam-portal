package com.onlineexamportal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryQuestionCountDto {
    int logicalQuestionCount;
    int technicalQuestionCount;
    int programmingQuestionCount;
}
