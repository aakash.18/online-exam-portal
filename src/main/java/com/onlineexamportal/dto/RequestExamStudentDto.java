package com.onlineexamportal.dto;

import com.onlineexamportal.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RequestExamStudentDto {
    private List<User> examStudentList;
    private List<Long> examStudentIdList;
}
