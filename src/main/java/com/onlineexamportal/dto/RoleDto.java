package com.onlineexamportal.dto;

import com.onlineexamportal.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class RoleDto {
    private Long roleId;
    private String userName;
    private String userRole;
}
