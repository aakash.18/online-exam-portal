package com.onlineexamportal.dto;
import com.onlineexamportal.enums.UserRole;
import lombok.*;
import java.util.List;

@Getter
@Setter
public class ResponseUserDto {
    private Long userId;
    private String userName;
    private String password;
    private String name;
    private UserRole userRole;

    private List<RequestResultDto> requestResultDtoList;
    private List<QuestionDto>questionDtoList;
    private List<ResponseExamDto> responseExamDtoList;

}
