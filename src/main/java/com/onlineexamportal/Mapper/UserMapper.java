package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.ResponseUserDto;
import com.onlineexamportal.dto.ResponseUserStudentDto;
import com.onlineexamportal.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface UserMapper {
//    @Mapping(source = "questionList",target = "questionDtoList")
//    @Mapping(source = "resultList",target = "resultDtoList")
    @Mapping(source = "user.examStudentList" , target = "responseExamDtoList")
    ResponseUserDto toDto(User user);
//    @Mapping(source = "questionDtoList",target = "questionList")
//    @Mapping(source = "resultDtoList",target = "resultList")
    ResponseUserStudentDto toResponseUserStudentDto (User user);
    User toEntity(ResponseUserDto responseUserDto);
}
