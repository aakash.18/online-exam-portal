package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.entities.Question;

public interface ResponseQuestionImageDto {
    ResponseQuestionImageDto toQuestionImageDto(Question question);
}
