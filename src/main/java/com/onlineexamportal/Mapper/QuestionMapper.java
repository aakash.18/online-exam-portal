package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.QuestionDto;
import com.onlineexamportal.dto.ResponseQuestionDto;
import com.onlineexamportal.entities.Question;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel="spring")
public interface QuestionMapper {

    @Mapping(source = "question.user.userId",target = "adminId")
    @Mapping(source = "question.category.categoryName", target = "typeOfCategory")

    @Mapping(source = "question.questionImage.imageUrl", target = "imageUrl")
    @Mapping(source = "question.questionImage.opt1ImageUrl", target = "opt1ImageUrl")
    @Mapping(source = "question.questionImage.opt2ImageUrl", target = "opt2ImageUrl")
    @Mapping(source = "question.questionImage.opt3ImageUrl", target = "opt3ImageUrl")
    @Mapping(source = "question.questionImage.opt4ImageUrl", target = "opt4ImageUrl")
    QuestionDto toDto(Question question);

    @Mapping(source = "question.user.userName",target = "admin")
    @Mapping(source = "question.category.categoryName", target = "typeOfCategory")

    @Mapping(source = "question.questionImage.imageUrl", target = "imageUrl")
    @Mapping(source = "question.questionImage.opt1ImageUrl", target = "opt1ImageUrl")
    @Mapping(source = "question.questionImage.opt2ImageUrl", target = "opt2ImageUrl")
    @Mapping(source = "question.questionImage.opt3ImageUrl", target = "opt3ImageUrl")
    @Mapping(source = "question.questionImage.opt4ImageUrl", target = "opt4ImageUrl")
    ResponseQuestionDto toResponseQuestionDto(Question question);

    List<ResponseQuestionDto> toResponseQuestionDtoList(List<Question> questionList);

    Question toEntity(QuestionDto questionDto);

}
