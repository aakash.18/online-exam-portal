package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.*;
import com.onlineexamportal.entities.Category;
import com.onlineexamportal.entities.Exam;
import com.onlineexamportal.entities.Question;
import com.onlineexamportal.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface ExamMapper {
    @Mapping(source = "exam.user.userName", target = "userName")
    @Mapping(source = "exam.resultList" ,target = "requestResultDtoList")
    @Mapping(source = "exam.logicalQuestionDifficulty" ,target = "logicalDifficulty")
    @Mapping(source = "exam.technicalQuestionDifficulty" ,target = "technicalDifficulty")
    @Mapping(source = "exam.programmingQuestionDifficulty" ,target = "programmingDifficulty")
    @Mapping(source = "exam.examQuestionList" , target = "questionDtoList")
    @Mapping(source = "exam.examStudentList" , target = "responseUserDtoList")
    ResponseExamDto toResponseExamDto(Exam exam);

    @Mapping(source = "question.user.userName",target = "admin")
    @Mapping(source = "question.category.categoryName", target = "typeOfCategory")
    ResponseQuestionDto toResponseQuestionDto(Question question);

    @Mapping(source = "exam.examStudentList" , target = "examStudentList")
    RequestExamStudentDto toRequestExamStudentDto (Exam exam);
    List<ResponseQuestionDto> toResponseQuestionDtoList(List<Question> questionList);

    RequestExamDto toRequestExamDto(Exam exam);

    Exam toRequestExamDto(RequestExamDto requestExamDto);
    Exam toResponseExamDto(ResponseExamDto responseExamDto);



}
