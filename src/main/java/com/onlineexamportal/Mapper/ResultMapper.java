package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.RequestResultDto;
import com.onlineexamportal.dto.ResponseResultDetailDto;
import com.onlineexamportal.dto.ResponseResultDto;
import com.onlineexamportal.entities.Result;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface ResultMapper {

    @Mapping(source = "user.userId" , target = "userId")
    @Mapping(source = "exam.examId",target = "examId")
    RequestResultDto toRequestResultDto(Result result);

    @Mapping(source = "user.userName",target = "responseResultDto.userName")
    @Mapping(source = "exam.examName",target = "responseExamDto.examName")
    @Mapping(source = "exam.totalMarks",target = "responseExamDto.totalMarks")
    @Mapping(source = "exam.examId",target = "responseExamDto.examId")
    @Mapping(source = "exam.user.userName",target = "responseExamDto.userName")
    @Mapping(source = "resultId",target = "responseResultDto.resultId")
    @Mapping(source = "logicalMarks",target = "responseResultDto.logicalMarks")
    @Mapping(source = "technicalMarks",target = "responseResultDto.technicalMarks")
    @Mapping(source = "programmingMarks",target = "responseResultDto.programmingMarks")
    @Mapping(source = "logicalStatus",target = "responseResultDto.logicalStatus")
    @Mapping(source = "technicalStatus",target = "responseResultDto.technicalStatus")
    @Mapping(source = "programmingStatus",target = "responseResultDto.programmingStatus")
    @Mapping(source = "status",target = "responseResultDto.status")
    @Mapping(source = "exam.examId",target = "responseResultDto.examId")
    ResponseResultDetailDto toResponseResultDetailDto(Result result);

    @Mapping(source = "user.name", target = "userName")
    @Mapping(source = "user.userId" , target = "userId")
    @Mapping(source = "exam.examId",target = "examId")
    ResponseResultDto toResponseResultDto(Result result);
    @Mapping(source = "userId" , target = "user.userId")
    @Mapping(source = "examId",target = "exam.examId")
    Result toRequestResultEntity(RequestResultDto requestResultDto);

    @Mapping(source ="userName" , target = "user.name")
    @Mapping(source ="userId"  , target =  "user.userId")
    @Mapping(source = "examId",target = "exam.examId")
    Result toResponseResultEntity(ResponseResultDto responseResultDto);



}
