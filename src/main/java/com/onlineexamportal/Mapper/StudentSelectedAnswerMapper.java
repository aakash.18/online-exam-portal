package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.RequestStudentSelectedAnswerDto;
import com.onlineexamportal.dto.ResponseSelectedAnswerByCategoryDto;
import com.onlineexamportal.dto.ResponseStudentSelectedAnswerDto;
import com.onlineexamportal.entities.StudentSelectedAnswer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface StudentSelectedAnswerMapper {
    @Mapping(source = "studentSelectedAnswer.exam.examId",target = "examId")
    @Mapping(source = "studentSelectedAnswer.user.userId",target = "userId")
    @Mapping(source = "studentSelectedAnswer.question.questionId",target = "questionId")
    ResponseStudentSelectedAnswerDto toResponseStudentSelectedAnswerDto(StudentSelectedAnswer studentSelectedAnswer);

    @Mapping(source = "studentSelectedAnswer.exam.examId",target = "examId")
    @Mapping(source = "studentSelectedAnswer.user.userId",target = "userId")
    @Mapping(source = "studentSelectedAnswer.question.questionId",target = "questionId")
    RequestStudentSelectedAnswerDto toRequestStudentSelectedAnswerDto(StudentSelectedAnswer studentSelectedAnswer);

    @Mapping(source = "studentSelectedAnswer.exam.examId",target = "examId")
    @Mapping(source = "studentSelectedAnswer.user",target = "userDto")
    @Mapping(source = "studentSelectedAnswer.question",target = "questionDto")
    ResponseSelectedAnswerByCategoryDto toResponseSelectedAnswerByCategoryDto(StudentSelectedAnswer studentSelectedAnswer);
}
