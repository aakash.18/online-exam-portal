package com.onlineexamportal.Mapper;

import com.onlineexamportal.dto.RequestCategoryDto;
import com.onlineexamportal.dto.ResponseCategoryDto;
import com.onlineexamportal.entities.Category;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public interface CategoryMapper {
    RequestCategoryDto toRequestCategoryDto(Category category);
    ResponseCategoryDto toResponseCategoryDto(Category category);

    Category toRequestEntity(RequestCategoryDto requestCategoryDto);
    Category toResponseEntity(ResponseCategoryDto responseCategoryDto);
}
